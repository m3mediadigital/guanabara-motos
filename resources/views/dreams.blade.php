<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Guanabara Motos - Honda Dream</title>
    
    <meta property="og:url" content="@yield('PAGE_URL', request()->fullUrl())">
    <meta property="og:type" content="{{ env('OG_TYPE', 'website') }}">
    <meta property="og:title" content="{{ @$settings['meta_title'] }}">
    <meta property="og:image" content="@yield('PAGE_IMAGE')">
    <meta property="og:description" content="{{ $settings['meta_description'] }}">
    <meta property="og:site_name" content="{{ @$settings['meta_title'] }}">
    <meta property="og:locale" content="pt_BR">
    <meta itemprop="name" content="{{ @$settings['meta_title'] }}">
    <meta itemprop="description" content="{{ $settings['meta_description'] }}">
    <meta itemprop="image" content="@yield('PAGE_IMAGE')">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@yield('TWITTER_ACCOUNT', env('TWITTER_ACCOUNT'))">
    <meta name="twitter:url" content="@yield('PAGE_URL', request()->fullUrl())">
    <meta name="twitter:title" content="{{ @$settings['meta_title'] }}">
    <meta name="twitter:description" content="{{ $settings['meta_description'] }}">
    <meta name="twitter:image" content="@yield('PAGE_IMAGE')">

    <link rel="stylesheet" href="{{ asset('css/dream.css') }}">
</head>
<body>
    <header class="header mb-2">
        <div class="container pb-5">
            <div class="row d-flex justify-content-center p-5">
                <img src="{{ asset('images/logo.png') }}" class="logo" alt="{{ env('APP_NAME') }}">
            </div>
            <div class="row">
                <h1 class="text-uppercase">escolha a <strong>honda que <br> combina com você!</strong></h1>
            </div>
            <div class="row d-flex justify-content-center pb-5">
                <a href="#dream" class="link">
                    <img src="{{ asset('images/icons/mouse.svg') }}" class="mouse d-none d-lg-block" alt="mouse">
                    <img src="{{ asset('images/icons/set-mouse.svg') }}" class="mouse-set d-none d-lg-block position-absolute" alt="mouse">
                    <img src="{{ asset('images/icons/mouse-mobile.svg') }}" class="mouse-set d-lg-none" alt="mouse">
                </a>
            </div>
        </div>
    </header>

    <section id="dream">
        <div id="offers" class="owl-carousel owl-theme">
            @foreach ($dreams as $item)
                <div class="item" style="background-image: linear-gradient(#0000009e, #0000009e), url({{ $item->banner_dream }})">
                    <div class="container">
                        <h1 class="pb-5 d-flex align-items-center">{{ $item->model->complete_name }}</h1>
                        <a onclick="javascript:location.href='showroom/{{ $item->model->slug }}'" class="btn btn-danger text-uppercase">
                            eu quero
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    </section>

    <footer class="pt-5 pb-0 mb-0 footer">
        <div class="container-fluid">
            <h3 class="text-center text-uppercase pb-5">
                <strong>outros modelos</strong>
            </h3>
            <div class="row d-lg-none">
                <div id="offers-mobile" class="owl-carousel owl-theme">
                    @foreach ($dreams as $key => $item)
                        <a href="#dream" class="link" data-slide-to="{{ $key }}">
                            <div class="item" style="background-image: url({{ $item->banner_dream }})">
                                <div class="container d-flex justify-content-center align-items-center p-5">
                                    <h1>{{ $item->model->complete_name }}</h1>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="d-none d-lg-block">
                <div class="row">
                    @foreach ($dreams as $key => $item)
                        <div class="col-md-4 pb-5">     
                            <a href="#dream" class="link" data-slide-to="{{ $key }}">                       
                                <div class="item" style="background-image: url({{ $item->banner_dream }})">
                                    <div class="container d-flex justify-content-center align-items-center p-5">
                                        <h1>{{ $item->model->complete_name }}</h1>                                        
                                    </div>                                
                                </div>
                            </a>                         
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-12 col-md-6">
                    <img src="{{ asset('images/logo.png') }}" width="200px" alt="{{ env('APP_NAME') }}">
                </div>
                <div class="col-12 col-md-6 text-right">
                    <p class="pt-2">Desenvolvido por <strong><a href="https://novam3.com.br">Nova M3</a> </strong></p>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $("#offers").owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                items: 1,
                dots: false,
                autoplay:true,
                autoplayTimeout:5000,
            });

            $("#offers-mobile").owlCarousel({
                loop: true,
                margin: 10,
                nav: false,
                items: 1,
                autoplay:true,
                autoplayTimeout:3000,
                dots: false,
                responsiveClass:true,
                responsive:{
                    768:{
                        items:3,
                    }
                }
            });

            $('.footer .link, #offers').bind('click', function(e){
                e.preventDefault();
                if($(this).hasClass('prev')){
                    $('#offers').trigger('prev.owl');
                }else if($(this).hasClass('next')){
                    $('#offers').trigger('next.owl');
                }else{
                    let slideTo = $(this).data('slide-to');
                    $('#offers').trigger('to.owl.carousel', slideTo);
                }
            });

            $('.link').on('click', ()=>{
                $('html, body').animate({
                    scrollTop: $('#dream').offset().top
                }, 500);
            })
        }) 
    </script>
</body>
</html>