<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-N2GGC2N4Z5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-N2GGC2N4Z5');
    </script>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Guanabara Moto - Concessionária Honda" />
    <meta name="keywords"
        content=" cg 160, nxr 160 bros, pop 110i, biz 110i, biz 125, cg cargo, cg 125, fan, cg 160 start, titan, pcx, twister, bros, xre, pernambuco, concessionária honda, honda" />
    <meta name="facebook-domain-verification" content="yvy5wgqcupnjg7ny5eabfbi7rexxcw" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Guanabara Moto - Concessionária Honda" />
    <meta property="og:description" content="Guanabara Moto - Concessionária Honda" />
    <meta property="og:url" content="www.guanabaramotos.com.br" />
    <meta property="og:site_name" content="Guanabara Moto - Concessionária Honda" />
    <meta property="og:image" content="" />
    <meta property="og:locale" content="pt_BR" />

    <title>Guanabara Motos - Concessionária Honda</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body>
    <div id="root"></div>
    <script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script> --}}
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>

    <script type="text/javascript">
        var GVP_AccountKey = "TvJL06on0oqb/TLTUSk9fiJtmpyRJT/y_TbjU/fVaSQQ=_HyH2GxthMls=",
            GVP_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s2 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = "https://app.dialugo.com/embed/monitoramento.js";
            s1.charset = "UTF-8";
            s2.src = "https://embed.dialugo.com/jquery.min.js";
            s0.parentNode.insertBefore(s2, s0);
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
</body>

</html>
