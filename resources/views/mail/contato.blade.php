
@if(isset($requestData['descomplicar']) && $requestData['descomplicar'] == false)
	@component('mail::message')
@endif

<p>Você recebeu uma mensagem através do seu site.</p>
@if( !empty($requestData['nome']) )
<p><strong>Nome:</strong><br/> {{ $requestData['nome'] }}</p>
@endif

@if( !empty($requestData['email']) )
<p><strong>Email:</strong><br/> {{ $requestData['email'] }}</p>
@endif

@if( !empty($requestData['telefone']) )
<p><strong>Telefone:</strong><br/> {{ $requestData['telefone'] }}</p>
@endif

@if( !empty($requestData['whatsapp']) )
<p><strong>Celular/Whatsapp:</strong><br/> {{ $requestData['whatsapp'] }}</p>
@endif

@if( isset($requestData['oferta']) || isset($requestData->oferta ))
<p><strong>Veículo:</strong><br/> {{ isset($requestData['oferta']) ? $requestData['oferta']->full_name : $requestData->oferta->full_name }}</p>
@endif

@if( isset($requestData['novo']) || isset($requestData->novo ))
<p><strong>Veículo:</strong><br/> {{ isset($requestData['novo']) ? $requestData['novo']->model->complete_name : $requestData->novo->model->complete_name }}</p>
@endif

@if( isset($requestData['seminovo']) || isset($requestData->seminovo ))
<p><strong>Veículo:</strong><br/> {{ isset($requestData['seminovo']) ? $requestData['seminovo'] : '' }}</p>
@endif

@if( isset($requestData['consorcio']) || isset($requestData->consorcio ))
<p><strong>Veículo:</strong><br/> {{ isset($requestData['consorcio']) ? $requestData['consorcio']->model->complete_name : $requestData->consorcio->model->complete_name }}</p>
@endif

@if( isset($requestData['carro']) || isset($requestData->carro ))
<p><strong>Veículo:</strong><br/> {{ $requestData['carro'] }}</p>
@endif

@if( isset($requestData['produto']) || isset($requestData->produto))
<p><strong>Veículo:</strong><br/> {{ isset($requestData['produto']) ? $requestData['produto']->model->complete_name .' - '. $requestData['produto']->version->complete_name  : $requestData->produto->model->complete_name .' - '. $requestData->produto->version->complete_name  }}</p>
@endif

@if( isset($requestData['pecas']))
<p><strong>Produto:</strong><br/> {{ $requestData['pecas'] }}</p>
@endif

@if( isset($requestData['servico']) || isset($requestData->servico))
<p><strong>Serviço:</strong><br/> {{ $requestData['servico']}}</p>
@endif

@if( !empty($requestData['pagamentos']))
<p><strong>Tipo de Pagamento:</strong><br/> {{ $requestData['pagamentos'] }}</p>
@endif


@if( !empty($requestData['plano_nome']) )
<p><strong>Plano:</strong><br/> {{ $requestData['plano_nome'] }}</p>
@endif

@if( !empty($requestData['parcelas']) )
<p><strong>Parcelas:</strong><br/> {{ $requestData['parcelas'] }}</p>
@endif

@if( !empty($requestData['valor_parcela']) )
<p><strong>Parcelas:</strong><br/> {{ $requestData['valor_parcela'] }}</p>
@endif

@if( !empty($requestData['placa']) )
<p><strong>Placa:</strong><br/> {{ $requestData['placa'] }}</p>
@endif

@if( !empty($requestData['km']) )
<p><strong>Km:</strong><br/> {{ $requestData['km'] }} Km</p>
@endif

@if( !empty($requestData['ano']) )
<p><strong>Ano:</strong><br/> {{ $requestData['ano'] }}</p>
@endif

@if( !empty($requestData['assunto']) )
<p><strong>Assunto:</strong><br/> {{ ucfirst($requestData['assunto']) }}</p>
@endif

@if( !empty($requestData['hora1']) )
<p><strong>Horário desejado 1:</strong><br/> {{ ucfirst($requestData['hora1']) }}</p>
@endif

@if( !empty($requestData['data1']) )
<p><strong>Data desejada 1:</strong><br/> {{ ucfirst($requestData['data1']) }}</p>
@endif

@if( !empty($requestData['hora2']) )
<p><strong>Horário desejado 2:</strong><br/> {{ ucfirst($requestData['hora2']) }}</p>
@endif

@if( !empty($requestData['data2']) )
<p><strong>Data desejada 2:</strong><br/> {{ ucfirst($requestData['data2']) }}</p>
@endif

@if( !empty($requestData['modelo']) )
<p><strong>Modelo:</strong><br/> {{ ucfirst($requestData['modelo']) }}</p>
@endif

@if( !empty($requestData['fabricante']) )
<p><strong>Fabricante:</strong><br/> {{ ucfirst($requestData['fabricante']) }}</p>
@endif

@if( !empty($requestData['finalidade_1']) )
<p><strong>Finalidade 1:</strong><br/> {{ ucfirst($requestData['finalidade_1']) }}</p>
@endif

@if( !empty($requestData['finalidade_2']) )
<p><strong>Finalidade 2:</strong><br/> {{ ucfirst($requestData['finalidade_2']) }}</p>
@endif

@if( !empty($requestData['finalidade_3']) )
<p><strong>Finalidade 3:</strong><br/> {{ ucfirst($requestData['finalidade_3']) }}</p>
@endif

@if( !empty($requestData['data_nascimento']) )
<p><strong>Data de nascimento:</strong><br/> {{ ucfirst($requestData['data_nascimento']) }}</p>
@endif

@if( !empty($requestData['genero']) )
<p><strong>Genero:</strong><br/> {{ ucfirst($requestData['genero']) }}</p>
@endif

@if( !empty($requestData['idade_cnh']) )
<p><strong>Idade CNH:</strong><br/> {{ ucfirst($requestData['idade_cnh']) }}</p>
@endif

@if( !empty($requestData['faixa_etaria']) )
<p><strong>Faixa etária:</strong><br/> {{ ucfirst($requestData['faixa_etaria']) }}</p>
@endif

@if( !empty($requestData['cidade']) )
<p><strong>Cidade:</strong><br/> {{ ucfirst($requestData['cidade']) }}</p>
@endif

@if( !empty($requestData['mensagem']) )
<p><strong>Mensagem:</strong><br/> {{ ucfirst($requestData['mensagem']) }}</p>
@endif

@if(isset($requestData['descomplicar']) && $requestData['descomplicar'] == false)
@endcomponent
@endif


