import api from "../services/api";
import queryString from 'query-string'
import history from '../main/History'
import fireError from './fireError'
import fireSuccess from './fireSuccess'

const get = (
    method,
    slug,
    dispatch,
    singular,
    list_url
) => {
    api
        .get(method, {
            params: {
                slug: slug
            }
        })
        .then(response => {
            dispatch({
                type: `SET_${singular}`,
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error)
            fireError('Registro não encontrado.')
            // alert('Registro não encontrado.')
        })
}

const list = (
    per_page,
    page,
    method,
    dispatch,
    plural
) => {
    let {
        col,
        dir
    } = queryString.parse(history.location.search)

    if (typeof page !== 'number' || isNaN(page)) {
        page = 0
    }

    let order = {}
    order[col] = dir

    let offset = per_page * (page - 1) + per_page

    Object.assign({
        order,
        per_page,
        offset
    })

    api
        .get(method, {
            params: {
                limit: per_page
            }
        })
        .then(response => {
            // console.log(response);
            dispatch({
                type: `SET_${plural}`,
                payload: response.data
            })

            dispatch({
                type: `SET_${plural}_META`,
                payload: response.data
            })
        })
        .catch(error => {
            fireError('Ocorreu um erro ao processar sua solicitação.')
        })
}

const add = (
    method,
    variables,
    dispatch,
    singular
) => {
    console.log('nani')
    api
        .post(method, variables, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(response => {
            if (response.data.error) {

                fireError('Por favor,  verifique o preenchimento dos campos.')
                dispatch({
                    type: `SET_ERROR_${singular}`,
                    payload: response.data.error
                })
            } else {

                dispatch({
                    type: `SET_SUCCESS_${singular}`,
                    payload: response.data
                })
                fireSuccess('Registro inserido com sucesso.')
            }
        })
        .catch(error => {
            console.log(error)
            // console.log('simplecrud add error', error)
            fireError('Ocorreu um erro ao processar sua solicitação.')
        })
}

export default {
    get,
    list,
    add
}
