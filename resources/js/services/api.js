import axios from "axios";

const api = axios.create({
    //  baseURL: "https://api-descomplicar-moto.herokuapp.com/api/",
    baseURL: "/api/",
    // baseURL: window.location.hostname === 'localhost' ? "http://localhost:8000/api/" : 'https://api-descomplicar-moto.herokuapp.com/api/'
    // baseURL: window.location.hostname === 'localhost' ? 'https://api-descomplicar-moto.herokuapp.com/api/' : "http://localhost:8000/api/"
});

api.interceptors.request.use(async (config) => {
    config.headers["Access-Control-Allow-Origin"] = "*";
    // config.headers['Content-Type'] = 'multipart/form-data';
    return config;
});

export default api;
