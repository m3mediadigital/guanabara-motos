import Swal from "sweetalert2";

export default msg => {
  Swal.fire({
    title: 'Erro!',
    text: typeof msg !== 'undefined' ? msg : 'Ocorreu um erro ao processar sua solicitação.',
    type: 'error'
  })
}
