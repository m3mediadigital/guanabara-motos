import React from "react";
// import { SiTiktok } from 'react-icons/si';
import { SiTiktok } from 'react-icons/si';

export default ({item}) => {
    return (
        <div className="row">
            <div className="col-8 offset-2 d-none d-lg-flex justify-content-center">
                <div className="follow">
                    <h1 className="title">siga-nos</h1>
                    <div className="row-buttons">
                        {item.get("facebook") && (
                            <a href={item.get("facebook")} target="_blank" rel="noopener noreferrer" className="button-facebook">
                                <i className="fa fa-facebook-f" /> GuanabaraMotos
                            </a>
                        )}
                        {item.get("instagram") && (
                            <a href={item.get("instagram")} target="_blank" rel="noopener noreferrer" className="button-instagram">
                                <i className="fa fa-instagram" /> GuanabaraMotos
                            </a>
                        )}
                        {/* <a href={item.get("facebook")} target="_blank" rel="noopener noreferrer" className="button-facebook">
                            <i className="fa fa-facebook-f"/> GuanabaraMotos
                        </a> */}
                        {/* <a href={item.get("instagram")} target="_blank" rel="noopener noreferrer" className="button-instagram">
                            <i className="fa fa-instagram" /> GuanabaraMotos
                        </a> */}
                        {/* <a href="/" target="_blank" rel="noopener noreferrer" className="button-twitter">
                            <i className="fa fa-twitter" />PernambucoMotos
                        </a> */}
                        {/* {item.get("facebook") && (
                            <a href={item.get("facebook")} target="_blank" rel="noopener noreferrer" className="button-facebook">
                                <i className="fa fa-facebook-f" /> PernambucoMotos
                            </a>
                        )} */} 
                        {item.get("youtube") && (
                            <a href={item.get('youtube')} target="_blank" rel="noopener noreferrer" className="button-youtube">
                                <i className="fab fa-youtube" /> GuanabaraMotos
                            </a>
                        )}      
                        {item.get("tiktok") && (
                            <a href={item.get("tiktok")} target="_blank" rel="noopener noreferrer" className="button-tiktok">
                                <SiTiktok /> GuanabaraMotos
                            </a>                       
                        )}               
                        {/* {item.get("twitter") && (
                            <a href={item.get("twitter")} target="_blank" rel="noopener noreferrer" className="button-twitter">
                                <i className="fa fa-twitter" /> Guanabara
                            </a>
                        )} */}
                    </div>
                </div>
            </div>
        </div>
    );
};
