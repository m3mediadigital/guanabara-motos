import React, {Component} from "react";
import Payments from "../helpers/Payments";

const tag_color = {
    pcd: {
        color: "#154DA1",
        label: "PCD",
    },
    micro_empresario: {
        color: "#AB1A1A",
        label: "Micro Empresário",
    },
    produtor_rural: {
        color: "#6E451C",
        label: "Produtor Rural",
    },
    produtor_rural_mda: {
        color: "#6E451C",
        label: "Produtor Rural MDA",
    },

    taxista: {
        color: "#154DA1",
        label: "Taxista",
    },
    frotista: {
        color: "#AB1A1A",
        label: "Frotista",
    },
    locadora: {
        color: "#6E451C",
        label: "Produtor RuraLocadoral",
    },

    auto_escola: {
        color: "#154DA1",
        label: "Auto Escola",
    },
    motorista_aplicativo: {
        color: "#AB1A1A",
        label: "Motorista de Aplicativo",
    },
    escolar_autônomo: {
        color: "#6E451C",
        label: "Escolar e Autônomo",
    },
    corpo_diplomatico: {
        color: "#154DA1",
        label: "Corpo Diplomático",
    },
    grandes_frotistas: {
        color: "#154DA1",
        label: "Grandes Frotistas",
    },
    governo: {
        color: "#154DA1",
        label: "Governo",
    },

    deficiente_fisico: {
        color: "#154DA1",
        label: "Deficiênte Fisíco",
    },
    meepp: {
        color: "#154DA1",
        label: "ME / EPP",
    },
};

export default class DirectSaleBox extends Component {
    render() {
        const {item} = this.props;
        // let name = "meepp";
        return (
            <div className="col-12 col-sm-6 col-lg-3 mb-5 mt-3">
                <div className="item-direct-sales">
                    <a href={"/venda-direta/" + item.get("slug")}>
                        <span className="tag" style={{backgroundColor: tag_color[item.get("corporate_sales_category") === "me_/_epp" ? "meepp" : item.get("corporate_sales_category")].color}}>
                            {tag_color[item.get("corporate_sales_category") === "me_/_epp" ? "meepp" : item.get("corporate_sales_category")].label}
                        </span>
                        <h1>{item.getIn(["model", "brand", "name"]) + " " + item.getIn(["model", "complete_name"])}</h1>
                        <p> {item.getIn(["version", "name"])}</p>
                        <img src={item.getIn(["color", "image"]) ? item.getIn(["color", "image"]) : item.getIn(["model", "image"])} alt={item.getIn(["model", "brand", "name"]) + " " + item.getIn(["model", "complete_name"])} className="img-fluid" />
                        {item.get("value_from") ? (
                            <div>
                                {/* <span className="description">A partir de:</span> */}
                                <div className="price">
                                    <Payments car={item} />
                                </div>
                            </div>
                        ) : (
                            <span className="description">Consulte-nos</span>
                        )}
                        <div className="more">Tenho Interesse</div>
                    </a>
                </div>
            </div>
        );
    }
}
