import React, {Component} from "react";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {sendContato} from "../../actions/ContatosAction";

import InputMask from "react-input-mask";
import ScriptsSend from "../helpers/ScriptsSend";

class Contato extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            email: "",
            // pcd: "",
            telefone: "",
            mensagem: "",
            meio_contato: "",
            pagamento: "",
            sending: false,
            buttonEnabled: true,
            buttonText: "Enviar Proposta",
        };
        this.method = this.props.form;
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    clearForm(field) {
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state[field] = field === "sending" ? false : "";
        return this.state[field];
    }
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value,
        });
    }
    hasErrorFor(field) {
        return !!this.props.error_contato.get(field);
    }
    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            this.setState({
                sending: false,
                buttonEnabled: true,
                buttonText: "Enviar",
            });
            return (
                <span className="error">
                    <strong>{this.props.error_contato.get(field).get(0)}</strong>
                </span>
            );
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        const {list_settings} = this.props;
        this.setState({
            sending: true,
            buttonEnabled: false,
            buttonText: "Enviando ...",
        });

        const formData = new FormData();
        formData.append("to_address", ScriptsSend.getEmailsByKey(list_settings, this.method));
        formData.append("form", this.props.form ? this.props.form : "contato");
        formData.append("nome", this.state.nome);
        formData.append("email", this.state.email);
        formData.append("telefone", this.state.telefone);
        formData.append("carro", this.props.carro);
        formData.append("mensagem", this.props.carro ? this.props.carro : this.state.mensagem);
        formData.append("assunto", `Formulario de ${this.props.subject ? this.props.subject : 'Contato'}`);
        formData.append("unidade", "Matriz");
        formData.append("meio_contato", this.state.meio_contato);
        formData.append("pagamento", this.state.pagamento);
        formData.append("unidade", "unidade");
        formData.append("idOferta", this.props.value ? this.props.value : null);
        formData.append("idSeminovo", this.props.idSeminovo ? this.props.idSeminovo : null);
        formData.append("idDirect", this.props.idDirect ? this.props.idDirect : null);
        formData.append("catalog", this.props.catalog ? this.props.catalog : null);

        this.props.sendContato(this.method, formData);
    }

    render() {
        const {success_contato} = this.props;
        console.log(this.props)
        return (
            <div className="page form-orcamento" id="tenho-interece">
                <div className="text-side text-center">
                    <h1 className="text-uppercase">
                        <span>ficou</span> interessado?
                    </h1>
                    <p>Faça uma cotação preenchendo os dados abaixo, entraremos em contato o mais rápido possível pelo meio de comunicação de sua preferencia!</p>
                </div>

                <form className="form-side" onSubmit={this.handleSubmit} method="post">
                    <div className="all-info">
                        <div className="form-row">
                            <div className="col-12 col-sm-8">
                                <input type="text" placeholder="Nome e Sobrenome" className={`form-control ${this.hasErrorFor("nome") ? "error" : ""}`} name="nome" value={success_contato.size > 0 ? this.clearForm("nome") : this.state.nome} onChange={this.handleChange} required />
                                {this.renderErrorFor("nome")}
                            </div>
                            <div className="col-12 col-sm-4">
                                <InputMask mask="(99) 99999-9999" type="tel" name="telefone" className={`form-control ${this.hasErrorFor("telefone") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("telefone") : this.state.telefone} placeholder="Telefone" onChange={this.handleChange} required />
                                {this.renderErrorFor("telefone")}
                            </div>
                            {/* <div className="col-12 col-sm-3">
                                <select name="pcd" id="pcd" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("pcd") ? "error" : ""}`}>
                                    <option value="">PCD?</option>
                                    <option value="Sim">Sim</option>
                                    <option value="Nao">Nao</option>
                                </select>
                                {this.renderErrorFor("pcd")}
                            </div> */}
                            <div className="col-12 col-sm-5">
                                <input type="teemailxt" name="email" placeholder="Email" className={`form-control ${this.hasErrorFor("email") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("email") : this.state.email} onChange={this.handleChange} required />
                                {this.renderErrorFor("email")}
                            </div>
                            <div className="col-12 col-sm-3">
                                <select name="pagamento" id="pagamento" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("pagamento") ? "error" : ""}`} required>
                                    <option value="">Forma de Pagamento</option>
                                    <option value="À vista">À vista</option>
                                    <option value="Financiado">Financiado</option>
                                </select>
                                {this.renderErrorFor("pagamento")}
                            </div>
                            <div className="col-12 col-sm-4">
                                <select name="meio_contato" id="meio_contato" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("meio_contato") ? "error" : ""}`} required>
                                    <option value="">Por onde deseja ser contatado?</option>
                                    <option value="Email">Email</option>
                                    <option value="Telefone">Telefone</option>
                                    <option value="Whatsapp">Whatsapp</option>
                                </select>
                                {this.renderErrorFor("meio_contato")}
                            </div>
                        </div>
                    </div>
                    <div className="pt-5 d-flex justify-content-center">
                        <button  disabled={!this.state.buttonEnabled}>{this.state.buttonText}</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list_settings: state.settings.get("list_settings"),
    error_contato: state.contatos.get("error_contato"),
    success_contato: state.contatos.get("success_contato"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({sendContato}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Contato);
