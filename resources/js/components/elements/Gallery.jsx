import React from "react";
import Slider from "react-slick";
import TitleSection from "./TitleSection";


const params = {
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: "linear",
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1000,
    nav: true,
};

const Gallery = ({item}) => {
    const image = item.get("gallery");
    return (
        <div className="gallery pt-5">
            <TitleSection title="Galeria" subtitle="" />
            <Slider {...params}>
                {image.size === 0
                    ? null
                    : image.map((item, index) => (
                        <div className="item" key={index}>
                            <img src={item.get("path")} className="img-fluid w-100" alt="Guanabara" />
                        </div>
                    ))}
            </Slider>
        </div>
    );
}

export default Gallery