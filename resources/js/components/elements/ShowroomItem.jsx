/* eslint-disable jsx-a11y/aria-proptypes */
import React, {Component} from "react";
import TitleSection from "./TitleSection";
import FormOrcarmento from "../elements/FormOrcarmento";
import Slider from "react-slick";
import Payments from "../helpers/Payments";

export default class ShowroomItem extends Component {

    render() {
        const {item_nova} = this.props;
        return (
            <>
                <img src={item_nova.get("banner_dream")} alt={item_nova.getIn(["model", "complete_name"])} className="img-fluid w-100" />
                <div className="page seminovo">
                    <div className="container">
                        <div className="pt-5 pb-5">
                            <Payments car={item_nova} reajuste={item_nova} />
                            <Accordion item={item_nova.get("model")} />
                            <Galleria item={item_nova.get("model")} />
                            <FormOrcarmento subject="Novos" form="send-nova" carro={item_nova.getIn(["model", "complete_name"])} />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const Galleria = ({item}) => {
    const image = item.get("gallery");
    return (
        <div className="gallery pt-5">
            <TitleSection title="Galeria" subtitle="" />
            <Slider {...params}>
                {image.size === 0
                    ? null
                    : image.map((item, index) => {
                          return (
                              <div className="item" key={index}>
                                  <img src={item.get("path")} className="img-fluid w-100" alt="Guanabara" />
                              </div>
                          );
                      })}
            </Slider>
        </div>
    );
};

const params = {
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: "linear",
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1000,
    nav: true,
};

const Accordion = ({item}) => {
    const sobre = item.get("highlight");
    return (
        <div className="accordion pb-5">
            <ul className="nav nav-pills mb-3 d-flex justify-content-between" id="pills-tab" role="tablist">
                {sobre.size === 0
                    ? null
                    : sobre.map((item, index) => {
                        return (
                            <li className="nav-item" key={index}>
                                <a className={`nav-link ${item.get("position") === 1 ? "active right-angle-arrow" : ""}`} id={`pills-${item.get("id")}-${item.get("models_id")}-tab`}
                                data-toggle="pill" href={`#pills-${item.get("id")}-${item.get("models_id")}`} role="tab" aria-controls={`pills-${item.get("id")}-${item.get("models_id")}`}
                                    aria-selected={`${item.position === 1 ? true : false}`}>
                                    {item.get("name")}
                                </a>
                            </li>
                        );
                      })}
                <li className="nav-item interesse">
                    <a className="nav-link" href="#tenho-interece">
                        tenho interesse
                    </a>
                </li>
            </ul>
            <div className="tab-content pt-2" id="pills-tabContent">
                {sobre.size === 0
                    ? null
                    : sobre.map((item, index) => {
                            return (
                                <div key={index} className={`tab-pane fade ${item.get("position") === 1 ? "show active" : ""}`} id={`pills-${item.get("id")}-${item.get("models_id")}`}
                                     role="tabpanel" aria-labelledby={`pills-${item.get("id")}${item.get("models_id")}-tab`}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-7">
                                            <div className="background" style={{backgroundImage: `linear-gradient(rgba(0,0,0,.2), rgba(0,0,0,0.5)),url(${item.get("image")})`}}>
                                                <p>
                                                    {item.get("subtitle")}
                                                </p>
                                            </div>
                                            <div className="bg"></div>
                                        </div>
                                        <div className="col-sm-12 col-md-5 d-flex align-items-center">
                                            <div dangerouslySetInnerHTML={{__html: item.get("content")}} />
                                        </div>
                                    </div>
                                </div>
                            );
                      })}
            </div>
        </div>
    );
};
