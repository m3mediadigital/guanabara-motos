import React, {Component} from 'react'


export default class UsedBox extends Component {
    price (value) {
        let formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'BRL',
        });
        let price = formatter.format(value).split('.')
        return price[0].replace(',', '.').replace('R$', '') +'<sup>,' + price[1]+'</sup>'
    }
    render() {
        const {item} = this.props;
        return (
            <div className="col-12 col-sm-6 col-lg-3 mb-5 mt-3" key={item.get('id')}>
                <div className="item-used">
                    <div className="image-holder" style={{backgroundImage: 'url(' + item.get('image_1') + ')'}}>
                        <img src={item.get('imagem_frontal')} alt="" className="sr-only"/>
                    </div>
                    <div className="body">
                        <span className="tag">
                            {item.get('fab_year')}
                        </span>
                        <h1>{item.getIn(['model','brand', 'name'])  + ' ' + item.getIn(['model', 'complete_name']) }</h1>
                        <p> {}</p>

                        {
                            item.get('used_value') ? (
                                <div>
                                    <span className="description">Por apenas R$</span>
                                    <span className="price" dangerouslySetInnerHTML={{__html: this.price(item.get('used_value'), {sanitize: true})}} />
                                </div>
                            ) : (
                                <span className="description">Consulte-nos</span>
                            )
                        }

                    </div>
                    <div className="info">
                        <div className="left">
                            {item.get('km') ? item.get('km') + 'KM' : 'Não informado'}
                        </div>
                        <div className="right">
                            {item.get('color')}
                        </div>
                    </div>
                    <a className="more" href={'/seminovo/' + item.get('slug')}>
                        Tenho Interesse
                    </a>
                </div>
            </div>
        )
    }
}
