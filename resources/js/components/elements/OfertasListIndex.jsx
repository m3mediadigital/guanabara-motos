import React, {Component} from "react";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getListOfertas} from "../../actions/OfertasAction";
import Loader from "react-loader-spinner";
import Payments from "../helpers/Payments";

import TitleSection from "../elements/TitleSection";

import Slider from "react-slick";
import { FaArrowLeft } from "react-icons/fa";
import { FaArrowRight } from "react-icons/fa";

class OfertasListaIndex extends Component {
    constructor(props) {
        super(props);
        this.getListOfertas();
    }

    getListOfertas() {
        this.props.getListOfertas(null, 0);
    }

    render() {
        const {list_ofertas} = this.props;

        function SampleNextArrow(props) {
            const { className, style, onClick } = props;
            return (
                <div
                    className={className}
                    style={{ ...style, display: "block", background: "red", borderRadius: "100%", padding: "4px" }}
                    onClick={onClick}
                >
                    <FaArrowLeft style={{ ...style, fontSize: "12px" }} />
                </div>
            );
        }

        function SamplePrevArrow(props) {
            const { className, style, onClick } = props;
            return (
                <div
                    className={className}
                    style={{ ...style, display: "block", background: "red", borderRadius: "100%", padding: "4px" }}
                    onClick={onClick}
                >
                    <FaArrowRight style={{ ...style, fontSize: "12px" }} />
                </div>
            );
        }

        const params = {
            // arrows: true,
            lazyLoad: true,
            autoplay: false,
            autoplaySpeed: 4000, //10seg
            cssEase: "linear",
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 1500,
            nextArrow: <SamplePrevArrow />,
            prevArrow: <SampleNextArrow />,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // arrows: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // arrows: true,
                    },
                },
            ],
        };

        return (
            <div className="page">
                <div className="row">
                    <div className="col-12">
                        <TitleSection title="Nossas" subtitle="Ofertas" />
                    </div>
                    <Slider {...params} className="w-100">
                        {list_ofertas.size === 0 ? (
                            <div>
                                <div className="loading p-5">
                                    <Loader type="Oval" color="#DB0000" height={100} width={100} />
                                    {/* <h2>Em breve super ofertas</h2> */}
                                </div>
                            </div>
                        ) : (
                            list_ofertas.map((item, index) => {
                                return (
                                    <div key={index} className="item">
                                        <div className="w-100 p-2" id={item.get("slug")}>
                                            <div className="item-direct-sales w-100">
                                                <a href={"/oferta/" + item.get("slug")}>
                                                    <h1>{item.getIn(["model", "complete_name"])} </h1>
                                                    <p>{item.getIn(["version", "complete_name"])}</p>
                                                    <img src={item.getIn(["model", "image"]) ? item.getIn(["model", "image"]) : item.getIn(["version", "image"])} alt="" className="img-fluid" />
                                                    <Payments car={item} />
                                                    {/* <img src={item.getIn(["selo","image"])} className="selo" alt={item.getIn(["model", "complete_name"])} /> */}
                                                </a>
                                                {/* <p className="more">Tenho Interesse</p> */}
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        )}
                    </Slider>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list_ofertas: state.ofertas.get("list_ofertas"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListOfertas}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OfertasListaIndex);
