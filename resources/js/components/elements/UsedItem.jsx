import React, {Component} from "react";
import TitleSection from "./TitleSection";

import Slider from "react-slick";
import FormOrcamento from "../elements/FormOrcarmento";

const params = {
    nav: true,
    autoplay: true,
    autoplaySpeed: 10000, //10seg
    cssEase: "linear",
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1500,
};

export default class UsedItem extends Component {
    price(value) {
        let formatter = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "BRL",
        });
        let price = formatter.format(value).split(".");
        return price[0].replace(",", ".").replace("R$", "") + "<sup>," + price[1] + "</sup>";
    }

    convertOptionals(value) {
        return Object.values(value.toArray());
    }

    render() {
        const {item_seminovo} = this.props;
        let optionals = item_seminovo != null && item_seminovo.size > 0 ? this.convertOptionals(item_seminovo.get("optionals")) : null;
        return (
            <>
                <div className="row">
                    <div className="col-12">
                        <TitleSection title="Seminovos" subtitle="de alto padrão" />
                    </div>
                    <div className="col-12 col-lg-6">
                        <Slider {...params}>
                            {item_seminovo.get("image_1") && (
                                <div className="item">
                                    <div className="bg-gallery" style={{backgroundImage: "url(" + item_seminovo.get("image_1") + ")"}}>
                                        <img src={item_seminovo.get("image_1")} alt={item_seminovo.getIn(["model", "complete_name"])} className="sr-only" />
                                    </div>
                                </div>
                            )}
                            {item_seminovo.get("image_2") && (
                                <div className="item">
                                    <div className="bg-gallery" style={{backgroundImage: "url(" + item_seminovo.get("image_2") + ")"}}>
                                        <img src={item_seminovo.get("image_2")} alt={item_seminovo.getIn(["model", "complete_name"])} className="sr-only" />
                                    </div>
                                </div>
                            )}
                            {item_seminovo.get("image_3") && (
                                <div className="item">
                                    <div className="bg-gallery" style={{backgroundImage: "url(" + item_seminovo.get("image_3") + ")"}}>
                                        <img src={item_seminovo.get("image_3")} alt={item_seminovo.getIn(["model", "complete_name"])} className="sr-only" />
                                    </div>
                                </div>
                            )}
                            {item_seminovo.get("image_4") && (
                                <div className="item">
                                    <div className="bg-gallery" style={{backgroundImage: "url(" + item_seminovo.get("image_4") + ")"}}>
                                        <img src={item_seminovo.get("image_4")} alt={item_seminovo.getIn(["model", "complete_name"])} className="sr-only" />
                                    </div>
                                </div>
                            )}
                            {item_seminovo.get("image_5") && (
                                <div className="item">
                                    <div className="bg-gallery" style={{backgroundImage: "url(" + item_seminovo.get("image_5") + ")"}}>
                                        <img src={item_seminovo.get("image_5")} alt={item_seminovo.getIn(["model", "complete_name"])} className="sr-only" />
                                    </div>
                                </div>
                            )}
                        </Slider>
                    </div>
                    <div className="col-12 col-lg-6 position-relative">
                        <div className="tag">{item_seminovo.get("fab_year")}</div>
                        <h1 className="title">{item_seminovo.getIn(["model", "brand", "name"]) + " " + item_seminovo.getIn(["model", "complete_name"])}</h1>
                        {item_seminovo.get("used_value") ? (
                            <div className="pt-5">
                                <span className="description">Por apenas R$</span>
                                <div className="price">
                                    <span className="price" dangerouslySetInnerHTML={{__html: this.price(item_seminovo.get("used_value"), {sanitize: true})}} />
                                </div>
                            </div>
                        ) : (
                            <span className="description">Consulte-nos</span>
                        )}
                        <div className="info">
                            <div className="left">{item_seminovo.get("km") ? item_seminovo.get("km") + "KM" : "Não informado"}</div>
                            <div className="right">{item_seminovo.get("color")}</div>
                        </div>
                        {/*<p>{item_seminovo.getIn(['model', 'description'])}</p>*/}
                        <div className="interesse-seminovo w-100">
                            <hr />
                            <div className="more">
                                <a href="#tenho-interece"> Tenho interesse</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="all-info">
                            <strong>Todos os itens de serie</strong>
                            <br />
                            <div>
                                {optionals != null
                                    ? optionals.map((value, key) => {
                                          return <span key={key}>{value[1]}, </span>;
                                      })
                                    : null}
                            </div>
                        </div>
                    </div>
                    <FormOrcamento form="seminovos" carro={item_seminovo.getIn(["model", "brand", "name"]) + " " + item_seminovo.getIn(["model", "complete_name"]) + " " + item_seminovo.get("model_year")} idSeminovo={item_seminovo.get("id")} />
                </div>
            </>
        );
    }
}
