import React, {Component} from 'react'


export default class PartBox extends Component {

    render() {
        console.log(this.props.items)
        return (
            <>
            {
                this.props.items.filter((categoria) => categoria.get("catalog_categories_id") === 49)
                .map((item, index) => {
                    return (
                        <div className="col-12 col-sm-6 col-lg-3 mb-5 mt-3" key={index}>
                            {item.get("custom_answers").map((custom, key) => {
                                return (
                                    <div className="item-part" key={key}>
                                        <div className="image-holder" style={{backgroundImage: 'url(' + item.get('image') + ')'}}>
                                            <img src={item.get('image')} alt="" className="sr-only"/>
                                        </div>
                                        <div className="body">
                                            <h1>{item.get('name')}</h1>
                                            <p>{item.get('description')}</p>
                                            {
                                                item.get('price') ? (
                                                    <div>
                                                        <span className="description">Por apenas R$</span>
                                                        <div className="price">
                                                            {item.get('price').substr(0, item.get('price').length - 3)}<sup>,{item.get('price').slice(-2)}</sup>
                                                        </div>
                                                    </div>
                                                ) : (
                                                    <span className="description">Consulte-nos</span>
                                                )
                                            }
                                        </div>
                                        <a className="more" target="_blank" rel="noopener noreferrer" href={custom.get("value")}>
                                            Tenho Interesse
                                        </a>
                                    </div>
                                )
                            })}
                        </div>
                    )
                })
            }
            </>
        )
    }
}
