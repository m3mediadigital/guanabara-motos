/* eslint-disable react/jsx-no-duplicate-props */
import React, {Component} from "react";
import TitleSection from "./TitleSection";
// import Payments from "../helpers/Payments";
import FormOrcamento from "../elements/FormOrcarmento";

export default class OfertaItem extends Component {
    price(value) {
        let formatter = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "BRL",
        });
        let price = formatter.format(value).split(".");
        return price[0].replace(",", ".").replace("R$", "") + "<sup>," + price[1] + "</sup>";
    }

    priceForm(value) {
        let price_full = value.substr(0, value.length - 3);
        let price_1 = price_full.substr(0, price_full.length - 3);
        let price_2 = price_full.slice(3);
        return price_1 + "." + price_2 + "," + value.slice(-2);
    }
    render() {
        const {item} = this.props;

        return (
            <div className="row">
                <div className="col-12 pb-5">
                    <TitleSection title="Serviços premium" subtitle="Honda" />
                </div>
                <div className="col-12 col-lg-6">
                    {/* <img src={item.get("image")} className="img-fluid" /> */}
                </div>
                <div className="col-12 col-lg-6 position-relative">
                    <div className="tag">{item.get("universal") === 1 ? "Universal" : ""}</div>
                    <h1 className="title">{item.get("name")}</h1>
                    {/* <p>{item.get("name")}</p> */}

                    {item.get("price") ? (
                        <div>
                            <span className="description">Por apenas R$</span> <br />
                            <span className="price" dangerouslySetInnerHTML={{__html: this.price(item.get("price"), {sanitize: true})}} />
                        </div>
                    ) : (
                        <span className="price">Consulte-nos</span>
                    )}

                    <div className="more">
                        <a href="#tenho-interece">Tenho interesse</a>
                    </div>
                </div>
                <div className="col-12 pt-5 pb-5">
                    <p>{item.get("description")}</p>
                </div>
                <FormOrcamento form="Pós-vendas" carro={item.getIn(["model", "complete_name"])} idAcessorio={item.get("id")} slug={item.get("slug")} carro={item.get("name") + " / valor: R$ " + this.price(item.get("price"))} catalog={item.get("name")} />
            </div>
        );
    }
}
