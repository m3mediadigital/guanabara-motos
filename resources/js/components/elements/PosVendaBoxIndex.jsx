import React, {Component} from "react";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import TitleSection from "../elements/TitleSection";

import {getListAcessorios} from "../../actions/AcessoriosAction";
import Loader from "react-loader-spinner";

import Slider from "react-slick";

class Servicos extends Component {
    constructor(props) {
        super(props);
        this.getListAcessorios();
    }

    getListAcessorios() {
        this.props.getListAcessorios(null, 0);
    }

    render() {
        const {list_acessorios} = this.props;
        const params = {
            lazyLoad: true,
            arrows: true,
            autoplay: true,
            autoplaySpeed: 4000, //10seg
            cssEase: "linear",
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 1500,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: false,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                    },
                },
            ],
        };

        return (
            <div className="container page Acessorios">
                <div className="row">
                    <div className="col-12">
                        <TitleSection title="Serviços premium" subtitle="Honda" />
                    </div>
                    <Slider {...params} className="w-100">
                        {list_acessorios.size === 0 ? (
                            <div className="loading p-5">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                                {/* <h2>Em breve super ofertas</h2> */}
                            </div>
                        ) : (
                            list_acessorios.map((item, index) => {
                                return <div key={index} className="p-2">
                                    <div className="item-service w-100">
                                        <div className="image-holder" style={{backgroundImage: "url(" + item.get("image") + ")"}}>
                                            <img src={item.get("image")} alt={item.get("name")} className="sr-only" />
                                        </div>
                                        <div className="body">
                                            <h1> {item.get("name")}</h1>
                                            {/* <p> {subtitle}</p> */}

                                            {item.get("price") ? (
                                                <div>
                                                    <span className="description">Por apenas R$</span>
                                                    <div className="price">
                                                        {item.get("price").substr(0, item.get("price").length - 2)}
                                                        <sup>{item.get("price").slice(-2)}</sup>
                                                    </div>
                                                    {/*<span className="price" dangerouslySetInnerHTML={{__html: this.price(price, {sanitize: true})}} />*/}
                                                </div>
                                            ) : (
                                                <span className="description">{item.get("decription")}</span>
                                            )}
                                        </div>
                                        <a className="more" href={"pos-venda/" + item.get("slug")}>
                                            Tenho Interesse
                                        </a>
                                    </div>
                                </div>;
                            })
                        )}
                    </Slider>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list_acessorios: state.acessorios.get("list_acessorios"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListAcessorios}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Servicos);
