// import React from "react";

// use date('N')
var _dias = {
    0: "Domingo",
    1: "Segunda-feira",
    2: "Terça-feira",
    3: "Quarta-feira",
    4: "Quinta-feira",
    5: "Sexta-feira",
    6: "Sábado"
};

// use data('n')
var _meses = {
    0: "Janeiro",
    1: "Fevereiro",
    2: "Março",
    3: "Abril",
    4: "Maio",
    5: "Junho",
    6: "Julho",
    7: "Agosto",
    8: "Setembro",
    9: "Outubro",
    10: "Novembro",
    11: "Dezembro"
};

/**
 * <p>Retorna o nome do dia da semana.</p>
 *
 * @param int number <p>Número do dia da semana. Use date('N') para obter.</p>
 * @param string type <p>Como você deseja que retorne o nome do dia da semana.<br/><b>'min'</b> para minificado (3 letras)<br/><b>'normal'</b> para o nome sem o sufixo '-feira'<br/><b>'full'</b> para o nome completo.</p>
 * @author Vilson Ferrari <vilson@m3mediadigital.com.br>
 */
function getDiaSemana(number, type = "normal") {
    if (_dias[number] !== "undefined") {
        let dia = _dias[number];

        if (type === "normal") {
            dia = dia.split("-").shift();
        } else if (type === "min") {
            dia = dia.substr(0, 3);
        }

        return dia;
    }
}

/**
 * <p>Retorna o nome do mês.</p>
 *
 * @param int number <p>Número do mês. Use date('n') para obter.</p>
 * @param string type <p>Como você deseja que retorne o nome do mês.<br/><b>'min'</b> para minificado (3 letras)<br/><b>'normal'</b> para o nome completo.</p>
 * @author Vilson Ferrari <vilson@m3mediadigital.com.br>
 */
function getMesNome(number, type = "normal") {
    if (_meses[number] !== "undefined") {
        let mes = _meses[number];

        if (type === "min") {
            mes = mes.substr(0, 3);
        }

        return mes;
    }
}

function agendamentos(daysOfPeriod = 30, timeOfWorking = 600, timeOfWorkingStep = 30) {
    let datas = [];
    let horas = [];
    for (var i = 1; i <= daysOfPeriod; i++) {
        // let time = strtotime('+' . i . 'days');
        let time = new Date();
        time.setDate(time.getDate() + i);
        let dateN = time.getDay();
        let dateD = time.getDate();
        let daten = time.getMonth();

        if (dateN < 7 && dateN > 0) {
            let diaSemana = this.getDiaSemana(dateN);
            let mesNome = this.getMesNome(daten);

            let label = diaSemana + ", " + dateD + " de " + mesNome;
            datas.push(label);
        }
    }

    // Se diminuir o valor final (600), o horário final
    // diminuirá junto. Por exemplo:
    // 600 = 18:00.
    // 300 = 13:00.
    for (var i = 0; i <= timeOfWorking; i += timeOfWorkingStep) {
        // Horário de inicio = 08:00.
        let time = new Date();
        time.setHours(8);
        time.setMinutes(0);
        time.setTime(time.getTime() + i * 60 * 1000);
        let hora = time.getHours();
        let minutos = time.getMinutes();

        if (hora <= 9) {
            hora = "0" + hora;
        }

        if (minutos <= 9) {
            minutos = "0" + minutos;
        }
        let horario = hora + ":" + minutos;
        horas.push(horario);
    }

    return {datas, horas};
}

export default {
    getDiaSemana,
    getMesNome,
    agendamentos
};
