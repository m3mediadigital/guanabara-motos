import React from "react";

const Payments = ({car, reajuste}) => {


    function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
        var padraoNova = 0
        if(reajuste) {
            if(reajuste.get("dream") === "Não") {
                padraoNova = 400
            }else {
                padraoNova = amount * 0.05
            }

        }
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? "-" : "";



            // car.get("dream") ? car.get("dream") : 'devia ser cilindrada'

            let i = parseInt((amount = Math.abs(Number(amount) + padraoNova || 0).toFixed(decimalCount))).toString();
            let j = i.length > 3 ? i.length % 3 : 0;

            return (
                negativeSign +
                (j ? i.substr(0, j) + thousands : "") +
                i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
                (decimalCount
                    ? decimal +
                      Math.abs(amount - i)
                          .toFixed(decimalCount)
                          .slice(2)
                    : "")
            );
        } catch (e) {
            console.log(e);
        }
    }

    if (car) {
        if (car.get("payment_method")) {
            // eslint-disable-next-line default-case
            switch (car.get("payment_method")) {
                case "bonus":
                    return (
                        <>
                            <p>
                                <strong>Bônus de</strong> R$
                            </p>
                            <span className="price">{formatMoney(car.get("bonus"))}</span>
                        </>
                    );

                case "parcel_no_entry":
                    return (
                        <>
                            <p>
                                <strong>Parcelas a partir de</strong> R$
                            </p>
                            <span className="price">{formatMoney(car.get("parcel_value"))}</span>
                        </>
                    );

                case "parcel_w_entry":
                    if (car.get("oferta_custom_tipo") === "Sim") {
                        return (
                            <>
                                <p>
                                    <strong>Entrada + </strong> {car.get("parcel_amount")}x de R$
                                </p>
                                <span className="price">{formatMoney(car.get("parcel_value"))}</span>
                            </>
                        );
                    } else {
                        return (
                            <>
                                <p>
                                    <strong>Entrada de R$ {formatMoney(car.get("entry"))} + </strong> {car.get("parcel_amount")}x de R$
                                </p>
                                <span className="price">{formatMoney(car.get("parcel_value"))}</span>
                            </>
                        );
                    }
                case "desconto":
                    if (typeof car.get("value_from") != "undefined" && car.get("value_from") && typeof car.get("value_to") != "undefined" && car.get("value_to")) {
                        return (
                            <>
                                <p>
                                    <strong>De R$ {formatMoney(car.get("value_from"))}</strong> Por R$
                                </p>
                                <span className="price">{formatMoney(car.get("value_to"))}</span>
                            </>
                        );
                    } else {
                        const value = typeof car.get("value_to") != "undefined" && car.get("value_to") ? car.get("value_to") : car.get("value_from");
                        if (value) {
                            return (
                                <>
                                    <p>
                                        <strong>A partir de</strong> R${" "}
                                    </p>
                                    <span className="price">{formatMoney(value)}</span>
                                </>
                            );
                        }
                    }
            }
        }
        if (car.get("used_value")) {
            return (
                <>
                    <p>
                        <strong>POR APENAS</strong> R$
                    </p>
                    <span className="price">{formatMoney(car.get("used_value"))}</span>
                </>
            );
        }

        if (car.get("value_from")) {
            return (
                <>
                    <p>
                        <strong>POR APENAS</strong> R$
                    </p>
                    <span className="price">{formatMoney(car.get("value_from"))}</span>
                </>
            );
        }

        if (car.get("price")) {
            return (
                <>
                    <p>
                        <strong>POR APENAS</strong> R$
                    </p>
                    <span className="price">{formatMoney(car.get("price"))}</span>
                </>
            );
        }

        if (car.get("brand_new_value")) {
            return (
                <>
                    <p>
                        <span>APARTIR DE</span> R$
                    </p>
                    <span className="price">{formatMoney(car.get("brand_new_value"))}</span>
                    <p>
                        {reajuste.get("dream") ? (reajuste.get("dream") === "Não" ? '+ CNH' : '+ CHN') : "" }
                    </p>
                </>
            );
        }
    } else {
        return <div></div>;
    }
};
export default Payments;
