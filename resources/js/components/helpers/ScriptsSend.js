// import {createNumberMask, createTextMask} from "redux-form-input-masks";
import {createTextMask} from "redux-form-input-masks";

function getFormSlug(settings, method) {
    let slug = method.replace("send-", "");
    if (settings.get("email_" + slug) != null) return slug;
    else return "contato";
}

function getEmailsByKey(settings, method) {
    let slug = method.replace("send-", "");
    if (settings.get("email_" + slug) != null) {
        let emails = settings.get("email_" + slug).replace("/s+/", "\n");
        return emails.split(",");
    }

    let emails = settings.get("email_contato").replace("/s+/", "\n");
    return emails.split(",");
}

const phoneMask = createTextMask({
    pattern: "(999) 999-9999",
});

export default {
    getFormSlug,
    getEmailsByKey,
    phoneMask,
};
