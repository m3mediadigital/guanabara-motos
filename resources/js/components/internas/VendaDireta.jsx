import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

// import TitleSection from "../elements/TitleSection";
import DirectSaleItem from "../elements/DirectSaleItem";
import {getVendaDireta} from "../../actions/VendasDiretasAction";
import Loader from "react-loader-spinner";

import Want from "../sections/Want";

class VendasDiretas extends Component {
    constructor(props) {
        super(props);
        const {slug} = this.props.match.params;

        if (slug) {
            this.props.getVendaDireta(slug);
        }
    }
    render() {
        const {item_venda_direta} = this.props;
        console.log(item_venda_direta);
        return (
            <Main>
                <div className="container seminovo page">
                    <div className="row">
                        {item_venda_direta != null && item_venda_direta.size > 0 ? (
                            <DirectSaleItem item={item_venda_direta} />
                        ) : (
                            <div className="loading">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                            </div>
                        )}
                    </div>
                </div>
                <div className="container">
                    <Want />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    item_venda_direta: state.vendas_diretas.get("item_venda_direta"),
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getVendaDireta,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(VendasDiretas);
