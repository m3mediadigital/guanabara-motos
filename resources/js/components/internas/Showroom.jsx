import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
// import Want from "../sections/Want";
import {getNova} from "../../actions/NovasAction";
import Loader from "react-loader-spinner";
import ShowroomItem from "../elements/ShowroomItem";

class Showroom extends Component {
    constructor(props) {
        super(props);
        const {slug} = this.props.match.params;

        if (slug) {
            this.props.getNova(slug);
        }
    }

    render() {
        const {item_nova} = this.props;

        return (
            <Main>
                <div className="">
                    {item_nova != null && item_nova.size > 0 ? (
                        <ShowroomItem item_nova={item_nova} />
                    ) : (
                        <div className="loading d-flex justify-content-center pt-5">
                            <Loader type="Oval" color="#DB0000" height={100} width={100} />
                        </div>
                    )}

                    {/* <div className="container page">
                        <Want />
                    </div> */}
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    item_nova: state.novas.get("item_nova"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getNova}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Showroom);
