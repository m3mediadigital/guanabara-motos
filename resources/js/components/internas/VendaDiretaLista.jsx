import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import TitleSection from "../elements/TitleSection";
import DirectSaleBox from "../elements/DirectSaleBox";
import {getVendasDiretas} from "../../actions/VendasDiretasAction";

import Want from "../sections/Want";
import Loader from "react-loader-spinner";

class VendasDiretas extends Component {
    constructor(props) {
        super(props);

        this.getVendasDiretas();
    }

    getVendasDiretas() {
        this.props.getVendasDiretas(null, 0);
    }
    render() {
        const {vendas_diretas} = this.props;
        console.log(vendas_diretas);
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <TitleSection title="Vendas diretas" subtitle="PCD, Produtor rural e MPE" />
                        </div>
                        {vendas_diretas.size === 0 ? (
                            <div className="loading">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                                {/* <div className="col-12">
                                    <h2 className="text-center">Em breve super ofertas</h2>
                                </div> */}
                            </div>
                        ) : (
                            vendas_diretas.map((item) => {
                                return item ? <DirectSaleBox item={item} /> : null;
                            })
                        )}
                    </div>
                </div>
                <div className="container">
                    <Want />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({vendas_diretas: state.vendas_diretas.get("list_vendas_diretas")});

const mapDispatchToProps = (dispatch) => bindActionCreators({getVendasDiretas}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(VendasDiretas);
