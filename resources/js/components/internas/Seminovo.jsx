import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import Want from "../sections/Want";
import UsedItem from "../elements/UsedItem";
import {getSeminovo} from "../../actions/SeminovosAction";
import Loader from "react-loader-spinner";

class Seminovo extends Component {
    constructor(props) {
        super(props);
        const {slug} = this.props.match.params;
        console.log(slug);
        if (slug) {
            this.props.getSeminovo(slug);
        }
    }

    render() {
        const {item_seminovo} = this.props;
        return (
            <Main>
                <div className="container page seminovo">
                    {item_seminovo != null && item_seminovo.size > 0 ? (
                        <UsedItem item_seminovo={item_seminovo} />
                    ) : (
                        <div className="loading text-center">
                            <Loader type="Oval" color="#DB0000" height={100} width={100} />
                        </div>
                    )}
                </div>
                <div className="container pt-5">
                    <Want />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    item_seminovo: state.seminovos.get("item_seminovo"),
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getSeminovo,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Seminovo);
