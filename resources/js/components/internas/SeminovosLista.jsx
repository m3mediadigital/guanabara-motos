import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import Want from "../sections/Want";
import TitleSection from "../elements/TitleSection";
import UsedBox from "../elements/UsedBox";
import {getListSeminovos} from "../../actions/SeminovosAction";

import Loader from "react-loader-spinner";

class SeminovosLista extends Component {
    constructor(props) {
        super(props);
        this.getListSeminovos();
    }

    getListSeminovos() {
        this.props.getListSeminovos(null, 0);
    }

    render() {
        const {list_seminovos} = this.props;
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <TitleSection title="Seminovos" subtitle="de alto padrão" />
                        </div>
                        {list_seminovos.size === 0 ? (
                            <div className="loading">
                                 <Loader type="Oval" color="#DB0000" height={100} width={100} />
                            </div>
                        ) : (
                            list_seminovos.map((item) => {
                                return item ? <UsedBox item={item} /> : null;
                            })
                        )}
                    </div>
                </div>
                <div className="container">
                    <Want />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_seminovos: state.seminovos.get("list_seminovos"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListSeminovos}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SeminovosLista);
