import React, {Component} from "react";
import Main from "../Main";

// import {connect} from "react-redux";
// import {bindActionCreators} from "redux";
import TitlePage from "../elements/TitlePage";
import imageQuemSomos from "../../assets/images/imageQuemSomos.png";

import Etica from '../../assets/images/Quem somos/etica.png';
import Qualidade from '../../assets/images/Quem somos/qualidade.png';
import Inovacao from '../../assets/images/Quem somos/inovacao.png';
import Transparencia from '../../assets/images/Quem somos/transparencia.png';
import Assertividade from '../../assets/images/Quem somos/assertividade.png';
import Disciplina from '../../assets/images/Quem somos/disciplina.png';
import Comprometimento from '../../assets/images/Quem somos/compromentimento.png';
import Integracao from '../../assets/images/Quem somos/integracao.png';
import Autodesenvolvimento from '../../assets/images/Quem somos/autodesenvolvimento.png';




// import {getInstitucional} from "../../actions/InstitucionalAction";

class QuemSomos extends Component {
    // constructor(props) {
    //     super(props);

    //     this.props.getInstitucional();
    // }
    render() {
        // const {list_institucional} = this.props;
        // console.log(list_institucional);
        return (
            <Main>
                <div className="container quem-somos">
                    <TitlePage title="Guanabara" subtitle="Motos" text="Quem somos, nossa história, missão, visão e valores" />

                    <br />
                    <div className="row mb-5">
                        <div className="col-8">
                            <img src={imageQuemSomos} alt="" className="img-fluid" />
                        </div>
                        <div className="col-4">
                            <div className="box-quem-somos">
                                <h3>Quem somos</h3>
                                <p>
                                    {/* {list_institucional &&
                                        list_institucional.map((item) => {
                                            if (item.get("slug") === "quem-somos") {
                                                return (
                                                    <div>
                                                        <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        <img className="img-equipe" src={item.get("imagem")} alt="" />
                                                    </div>
                                                );
                                            }
                                        })} */}
                                    Recém-chegada ao Estado do Rio de Janeiro, a Guanabara Motos é mais uma empresa do Grupo Loquipe, já referência de credibilidade, qualidade e satisfação em diversas áreas do ramo automotivo no nordeste e sudeste.
                                    Buscando estar sempre perto de você, possuímos matriz na Ilha do Governador, com mais cinco lojas e 10 pontos de venda espalhados pela Grande Rio.
                                    Trabalhamos com motocicletas novas e seminovas, nacionais e importadas, consórcio, produtos de força, peças e serviços de assistência técnica, tudo com o selo de qualidade Honda.
                                    Além dos números, nosso grupo tem como premissa a sustentabilidade e o meio ambiente.
                                    Viver sobre duas rodas é sinônimo de liberdade, e na Guanabara Motos você pode realizar este sonho!
                                    Fale conosco pelo WhatsApp e agende uma visita á nossa loja mais perto de você.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="row pt-3">
                        {/* <div className="col-4">
                            <h3>Princípios e Valores:</h3>
                            <p className="text-quem-somos">
                                { {list_institucional &&
                                    list_institucional.map((item) => {
                                        if (item.get("slug") === "principios-e-valores") {
                                            return (
                                                <div>
                                                    <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                </div>
                                            );
                                        }
                                    })} }
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </p>
                        </div> */}
                        <div className="col-6">
                            {/* <div className="box-quem-somos"> */}
                                <h3>Missão:</h3>
                                <p className="text-quem-somos">
                                    {/* {list_institucional &&
                                        list_institucional.map((item) => {
                                            if (item.get("slug") === "missao") {
                                                return (
                                                    <div>
                                                        <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                    </div>
                                                );
                                            }
                                        })} */}
                                Comercializar com excelência produtos e serviços para atender as necessidades de locomoção. Respeitando e satisfazendo clientes, colaboradores e fornecedores. Assegurando-lhes resultados que permitam a expansão e o aprimoramento das atividades.
                                </p>
                            {/* </div> */}
                        </div>
                        <div className="col-6">
                            {/* <div className="box-quem-somos"> */}
                                <h3>Visão:</h3>
                                <p className="text-quem-somos">
                                    {/* {list_institucional &&
                                        list_institucional.map((item) => {
                                            if (item.get("slug") === "visao") {
                                                return (
                                                    <div>
                                                        <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                    </div>
                                                );
                                            }
                                        })} */}
                                    Ser a melhor empresa em nosso segmento de atuação, reconhecida pelas soluções inovadoras e pelos princípios sustentáveis.
                                </p>
                            {/* </div> */}
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <h3>Valores:</h3>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Etica} alt="Ética"/>
                                        <h3 className="h3-text-quem-somos">Ética:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Atuamos de acordo com os princípios morais, respeitando nossos valores e honrando nosso compromisso de lealdade, confiabilidade, profissionalismo e honestidade junto à nossa organização e sociedade. Agimos sempre de forma integra!
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Qualidade} alt="Qualidade"/>
                                        <h3 className="h3-text-quem-somos">Qualidade:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                    Atuamos com foco na qualidade, de maneira que possamos reconhecer e atender às necessidades de nossos clientes. Cada colaborador deve atuar buscando melhoria nos processos internos, evitando desperdícios e otimizando resultados. Buscamos o melhor sempre!
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Inovacao} alt="Inovação"/>
                                        <h3 className="h3-text-quem-somos">Inovação:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Temos a capacidade de agregar valores ao que produzimos através de nossas ideias e conhecimentos. Pois sabemos que estimular a criatividade é garantir um diferencial competitivo. Pensando diferente, temos resultados diferentes.
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Transparencia} alt="Transparência"/>
                                        <h3 className="h3-text-quem-somos">Transparência:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Garantimos que todos conheçam nossas ações e os resultados da organização. Não temos o que esconder!
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Assertividade} alt="Assertividade"/>
                                        <h3 className="h3-text-quem-somos">Assertividade:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Asseguramos todos os procedimentos necessários ao cumprimento adequado das atividades laborais de cada função dentro de nossa organização, de modo que esta ação seja um facilitador para atuação dos setores que dependem de nossos resultados. Fazemos de nosso trabalho, um espelho de nossa competência.
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Disciplina} alt="Disciplina"/>
                                        <h3 className="h3-text-quem-somos">Disciplina:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Seguimos regras e políticas claramente expressas. Assumimos e cumprimos nossas obrigações e responsabilidades, buscando sempre o desenvolvimento de nossa empresa e de seus colaboradores. Somos cumpridores!
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Comprometimento} alt="Comprometimento"/>
                                        <h3 className="h3-text-quem-somos">Comprometimento:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Acreditamos profundamente no que pensamos e realizamos, irradiando energia, sentimento e percepções. Estimulando pessoas a buscarem seu desenvolvimento e alcançarem seus objetivos pessoais e os da Guanabara Motos. Buscamos sempre a melhor participação de cada colaborador.
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Integracao} alt="Integração"/>
                                        <h3 className="h3-text-quem-somos">Integração:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Zelamos pelo coletivo, tratando adequadamente nossa relação com os colaboradores internos e externos, visando alcançar objetivos comuns, compartilhando informações e auxiliando nossa equipe a desenvolver habilidades necessárias a atuação profissional. Só chegamos no resultado coletivamente!
                                    </p>
                                </div>
                            </div>
                            <div className="col-4">
                                <div className="">
                                    <div className="row institucionais-quem-somos">
                                        <img className="icons-quem-somos" src={Autodesenvolvimento} alt="Autodesenvolvimento"/>
                                        <h3 className="h3-text-quem-somos">Autodesenvolvimento:</h3>
                                    </div>
                                    <p className="text-quem-somos">
                                        {/* {list_institucional &&
                                            list_institucional.map((item) => {
                                                if (item.get("slug") === "missao") {
                                                    return (
                                                        <div>
                                                            <div dangerouslySetInnerHTML={{__html: item.get("conteudo")}} />
                                                        </div>
                                                    );
                                                }
                                            })} */}
                                        Temos ciência da necessidade de desenvolver nossas habilidades nas áreas em que atuamos. Dessa forma, seremos ainda mais capazes de alcançarmos nossos objetivos e os da organização. O aprendizado quebra barreiras e limites!
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

// const mapStateToProps = (state) => ({
//     list_institucional: state.institucional.get("list_institucional"),
// });

// const mapDispatchToProps = (dispatch) => bindActionCreators({getInstitucional}, dispatch);

// export default connect(mapStateToProps, mapDispatchToProps)(QuemSomos);
export default QuemSomos
