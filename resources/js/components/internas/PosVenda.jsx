import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import Want from "../sections/Want";
import PosVendaItem from "../elements/PosVendaItem";
import {getAcessorio} from "../../actions/AcessoriosAction";
import Loader from "react-loader-spinner";

class Seminovo extends Component {
    constructor(props) {
        super(props);
        const {slug} = this.props.match.params;
        if (slug) {
            this.props.getAcessorio(slug);
        }
    }

    render() {
        const {item_acessorio} = this.props;
        console.log(item_acessorio);
        return (
            <Main>
                <div className="container page seminovo">
                    {item_acessorio != null && item_acessorio.size > 0 ? (
                        <PosVendaItem item={item_acessorio} />
                    ) : (
                        <div className="loading text-center">
                            <Loader type="Oval" color="#DB0000" height={100} width={100} />
                        </div>
                    )}
                </div>
                <div className="container pt-5">
                    <Want />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    item_acessorio: state.acessorios.get("item_acessorio"),
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getAcessorio,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Seminovo);
