import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "react-loader-spinner";
import MonteBox from "../elements/MonteBox";
import InputMask from "react-input-mask";
import { getListConsorcios } from "../../actions/ConsorcioAction";
import {sendContato} from "../../actions/ContatosAction";
import Swal from "sweetalert2";
import ScriptsSend from "../helpers/ScriptsSend";

class Consorcio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            section: 1,

            vehicle: "",
            vehicle_name: null,
            vehicle_image: "",

            tipo_consorcio: "",
            parcela_min: "",
            parcela_max: "",

            nome: "",
            email: "",
            pcd: "",
            telefone: "",
            mensagem: "",
            meio_contato: "",
            sending: false,
        };
        this.getListNovas();
        this.method = "send-consorcio";
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    getListNovas() {
        this.props.getListConsorcios(null, 0);
    }
    setPage(page) {
        if (page < this.state.page) {
            this.setState({page: page});
        }
    }
    nextPage() {
        if (this.state.page === 1) {
            if (this.state.vehicle.length > 0) {
                this.setState({page: 2});
            } else {
                Swal.fire({
                    title: "Erro!",
                    text: "Selecione um modelo!",
                    type: "error",
                });
            }
        }
        if (this.state.page === 2) {
            if (this.state.parcela_min.length > 0 || this.state.parcela_max.length < 0 || (this.state.parcela_min.length < 0 && this.state.parcela_max.length > 0)) {
                this.setState({page: 3});
            } else {
                Swal.fire({
                    title: "Erro!",
                    text: "Selecione os valores da parcela!",
                    type: "error",
                });
            }
        }
    }
    clearForm(field) {
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state[field] = field === "sending" ? false : "";
        return this.state[field];
    }
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value,
        });
    }
    hasErrorFor(field) {
        return !!this.props.error_contato.get(field);
    }
    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            // eslint-disable-next-line react/no-direct-mutation-state
            this.state["sending"] = false;
            return (
                <span className="error">
                    <strong>{this.props.error_contato.get(field).get(0)}</strong>
                </span>
            );
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        const {list_settings} = this.props;
        this.setState({
            sending: true,
        });

        // eslint-disable-next-line no-useless-concat
        let msg = "Parcela mínima: " + this.state.parcela_min + " - " + "Parcela máxima : " + this.state.parcela_max + " - " + "Tipo consorcio : " + this.state.tipo_consorcio + " - " + "Veículo : " + this.state.vehicle_name;
        // this.setState({mensagem: msg});

        const formData = new FormData();
        formData.append("to_address", ScriptsSend.getEmailsByKey(list_settings, this.method));
        formData.append("form", "consorcios");
        formData.append("nome", this.state.nome);
        formData.append("email", this.state.email);
        formData.append("telefone", this.state.telefone);
        formData.append("mensagem", msg);
        formData.append("assunto", "Formulario de Consorcio");
        formData.append("unidade", "Matriz");
        formData.append("meio_contato", this.state.meio_contato);
        formData.append("idOferta", this.props.value ? this.props.value : null);
        formData.append("idSeminovo", this.props.idSeminovo ? this.props.idSeminovo : null);
        console.log(formData);

        this.props.sendContato(this.method, formData);
    }
    render() {
        const {list_consorcios, success_contato} = this.props;
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <h1>Consórcio Guanabara</h1>
                            <p>O jeito mais fácil de ter uma Honda 0KM</p>
                        </div>
                    </div>
                </div>
                <div className="tabs-holder">
                    <div className="container">
                        <ul className="row">
                            <li className={"col-3 tab nav-item" + (this.state.page === 1 ? " active border border-bottom-0" : "")} onClick={() => this.setPage(1)}>
                                <h1>1.</h1>
                                <p>Escolha o modelo</p>
                            </li>
                            <li className={"col-3 tab nav-item" + (this.state.page === 2 ? " active border border-bottom-0" : "")} onClick={() => this.setPage(2)}>
                                <h1>2.</h1>
                                <p>Disponibilidade</p>
                            </li>
                            <li className={"col-3 tab nav-item" + (this.state.page === 3 ? " active border border-bottom-0" : "")} onClick={() => this.setPage(3)}>
                                <h1>3.</h1>
                                <p>Identificação</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="bg-white">
                        <div className={"container page_1 " + (this.state.page === 1 ? " " : "d-none")}>
                            <div className="row">
                                {list_consorcios.size === 0 ? (
                                    <div className="loading mt-5 mb-5">
                                        <Loader type="Oval" color="#DB0000" height={100} width={100} />
                                    </div>
                                ) : (
                                    list_consorcios.map((item, index) => {
                                        return item ? (
                                            <MonteBox
                                                key={index}
                                                slug={item.get("slug")}
                                                title={item.getIn(["model", "complete_name"])}
                                                image={item.getIn(["model", "image"])}
                                                selected={this.state.vehicle === item.get("slug")}
                                                onClick={() => {
                                                    this.setState({vehicle: item.get("slug")});
                                                    this.setState({vehicle_image: item.getIn(["model", "image"])});
                                                    this.setState({vehicle_name: item.getIn(["model", "complete_name"])});
                                                }}
                                            />
                                        ) : null;
                                    })
                                )}
                            </div>
                        </div>

                        <div className={"container page_2 " + (this.state.page === 2 ? " " : "d-none")}>
                            <div className="row">
                                <div className="col-6 pt-5 pb-5">
                                    <div className="row">
                                        <div className="col-12">
                                            <strong>Como você quer simular?</strong>
                                        </div>
                                        <div className="form-group col-6">
                                            <label>
                                                <input type="radio" name="tipo_consorcio" value="parcela" onChange={this.handleChange} /> Quero escolher o valor da parcela
                                            </label>
                                        </div>
                                        <div className="form-group col-6">
                                            <label>
                                                <input type="radio" name="tipo_consorcio" value="credito" onChange={this.handleChange} /> Quero quanto de crédito
                                            </label>
                                        </div>
                                        <div className="form-group col-6">
                                            <label>
                                                <InputMask mask="R$ 999.999,99" type="text" name="parcela_min" className={`form-control ${this.hasErrorFor("parcela_min") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("parcela_min") : this.state.parcela_min} placeholder="Parcela mínima (R$)" onChange={this.handleChange} />
                                            </label>
                                        </div>
                                        <div className="form-group col-6">
                                            <label>
                                                <InputMask mask="R$ 999.999,99" type="text" name="parcela_max" className={`form-control ${this.hasErrorFor("parcela_max") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("parcela_max") : this.state.parcela_max} placeholder="Parcela máxima (R$)" onChange={this.handleChange} />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-6 pt-5 ">
                                    <img src={this.state.vehicle_image} alt="" className="img-fluid" />
                                </div>
                            </div>
                        </div>

                        <div className={"container page_3 " + (this.state.page === 3 ? "" : "d-none")}>
                            <div className="row">
                                <div className="col-12 col-lg-6">
                                    <form className="container mt-5 mb-5" onSubmit={this.handleSubmit} method="post">
                                        <div className="row">
                                            <div className="form-group col-12 ">
                                                <input type="text" placeholder="Nome e Sobrenome" className={`form-control ${this.hasErrorFor("nome") ? "error" : ""}`} name="nome" value={success_contato.size > 0 ? this.clearForm("nome") : this.state.nome} onChange={this.handleChange} />
                                                {this.renderErrorFor("nome")}
                                            </div>
                                            <div className="form-group col-12">
                                                <InputMask mask="(99) 99999-9999" type="text" name="telefone" className={`form-control ${this.hasErrorFor("telefone") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("telefone") : this.state.telefone} placeholder="Telefone" onChange={this.handleChange} />
                                                {this.renderErrorFor("telefone")}
                                            </div>
                                            <div className="form-group col-12">
                                                <input type="text" name="email" placeholder="Email" className={`form-control ${this.hasErrorFor("email") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("email") : this.state.email} onChange={this.handleChange} />
                                                {this.renderErrorFor("email")}
                                            </div>
                                            <div className="form-group col-12">
                                                <select name="meio_contato" id="meio_contato" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("meio_contato") ? "error" : ""}`}>
                                                    <option value="">Por onde deseja ser contatado?</option>
                                                    <option value="Email">Email</option>
                                                    <option value="Telefone">Telefone</option>
                                                    <option value="Whatsapp">Whatsapp</option>
                                                </select>
                                                {this.renderErrorFor("meio_contato")}
                                            </div>

                                            <div className="form-group offset-6 col-6">
                                                <button className="btn-send">Enviar mensagem</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-6 pt-5 ">
                                    <img src={this.state.vehicle_image} alt="" className="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="tabs-holder">
                    <div className="container">
                        <div className="row">
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Modelo Selecionado</span>
                                <strong>{this.state.vehicle_name ? this.state.vehicle_name : "--"} </strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Parcela</span>
                                <strong>{this.state.parcela_min ? this.state.parcela_min : "--"}</strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Nome</span>
                                <strong>{this.state.nome ? this.state.nome : "--"}</strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column justify-content-center pt-4">
                                <button className={"btn-send " + (this.state.page === 4 ? "d-none" : "")} onClick={() => this.nextPage()}>
                                    Avançar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_consorcios: state.consorcios.get("list_consorcios"),
    list_settings: state.settings.get("list_settings"),
    error_contato: state.contatos.get("error_contato"),
    success_contato: state.contatos.get("success_contato"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListConsorcios, sendContato}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Consorcio);
