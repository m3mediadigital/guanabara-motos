import React, { Component } from 'react'

class Passo3 extends Component {

    render () {
        if (this.props.currentStep !== 3) {
            return null
        }
        
        return(
            <div className="form-passo3">
                { this.props.success_revisao.size > 0 
                    ?   
                        <React.Fragment><i className="fa fa-check-square-o" aria-hidden="true"></i>
                        <div className="text">
                            <h2>REVISÃO AGENDADA COM SUCESSO!</h2>
                            <p>Um de nossos atendentes irá entrar em contato com você para confirmar os dados e o Agendamento do serviço.</p> 
                        </div></React.Fragment>
                    :   <React.Fragment><i className="fa fa-spinner fa-pulse fa-fw" aria-hidden="true"></i>
                        <div className="text">
                            <h2>Estamos processando sua solicitação!</h2>
                        </div></React.Fragment>
                }
                
            </div>
        ) 
    }
}

export default Passo3