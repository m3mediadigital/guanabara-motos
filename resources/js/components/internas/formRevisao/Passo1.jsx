import React, {Component} from "react";
import Calendario from "../../helpers/Calendario";

import InputMask from "react-input-mask";

class Passo1 extends Component {
    options(item) {
        let rows = [];
        for (var i in item) {
            rows.push(
                <option key={item[i]} value={item[i]}>
                    {item[i]}
                </option>
            );
        }
        return rows;
    }

    render() {
        const {datas, horas} = Calendario.agendamentos(30, 600, 30);
        if (this.props.currentStep !== 1) {
            return null;
        }

        return (
            <div className="form-passo1">
                <p>SEUS DADOS</p>
                <div className="form-row">
                    <div className="form-group col-md-4">
                        <input className="form-control" id="nome" name="nome" type="text" placeholder="Nome completo" value={this.props.nome} onChange={this.props.handleChange} />
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.nome !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                    <div className="form-group col-md-4">
                        <input className="form-control" id="email" name="email" type="email" placeholder="Email" value={this.props.email} onChange={this.props.handleChange} />
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.email !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                    <div className="form-group col-md-4">
                        <InputMask mask="(99) 99999-9999" className="form-control" id="telefone" name="telefone" type="text" placeholder="WhatsApp" value={this.props.telefone} onChange={this.props.handleChange} />
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.telefone !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                </div>
                <p>DADOS DA REVISÃO</p>
                <div className="form-row">
                    <div className="form-group col-md-2">
                        <select className="form-control" name="hora1" id="hora1" value={this.props.hora1} onChange={this.props.handleChange}>
                            <option value="" selected>
                                Horário 1
                            </option>
                            {this.options(horas)}
                        </select>
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.hora1 !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                    <div className="form-group col-md-4">
                        <select className="form-control" name="data1" id="data1" value={this.props.data1} onChange={this.props.handleChange}>
                            <option value="" selected>
                                Data 1
                            </option>
                            {this.options(datas)}
                        </select>
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.data1 !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                    <div className="form-group col-md-2">
                        <select className="form-control" name="hora2" id="hora2" value={this.props.hora2} onChange={this.props.handleChange}>
                            <option value="" selected>
                                Horário 2
                            </option>
                            {this.options(horas)}
                        </select>
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.hora2 !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                    <div className="form-group col-md-4">
                        <select className="form-control" name="data2" id="data2" value={this.props.data2} onChange={this.props.handleChange}>
                            <option value="" selected>
                                Data 2
                            </option>
                            {this.options(datas)}
                        </select>
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.data2 !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-md-6">
                        <input className="form-control" name="km" type="text" value={this.props.km} onChange={this.props.handleChange} placeholder="Revisão de: (KM)" />
                        <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.km !== "") ? "hide" : ""}`}>
                            <strong>Este campo é obrigatório</strong>
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Passo1;
