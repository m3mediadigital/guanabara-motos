import React, {Component} from "react";
import twister from "../../../assets/images/moto1.png";

class Passo2 extends Component {
    render() {
        if (this.props.currentStep !== 2) {
            return null;
        }

        return (
            <div className="form-passo2">
                <img src={twister} alt="" />
                <div className="form-revisao">
                    <p>SOBRE O VEÍCULO</p>
                    <div className="form-row">
                        <div className="form-group col-md-12">
                            <input className="form-control" id="modelo" name="modelo" type="text" placeholder="Modelo" value={this.props.modelo} onChange={this.props.handleChange} />
                            <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.modelo !== "") ? "hide" : ""}`}>
                                <strong>Este campo é obrigatório</strong>
                            </span>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-5">
                            <input className="form-control" name="ano" id="inputEstado" placeholder="Ano" value={this.props.ano} onChange={this.props.handleChange} />
                            <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.ano !== "") ? "hide" : ""}`}>
                                <strong>Este campo é obrigatório</strong>
                            </span>
                        </div>
                        <div className="form-group col-md-7">
                            <input className="form-control" id="placa" name="placa" type="text" placeholder="Placa" value={this.props.placa} onChange={this.props.handleChange} />
                            <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.placa !== "") ? "hide" : ""}`}>
                                <strong>Este campo é obrigatório</strong>
                            </span>
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-12">
                            <textarea className="form-control" id="mensagem" name="mensagem" type="text" placeholder="Mensagem" value={this.props.mensagem} onChange={this.props.handleChange}></textarea>
                            <span className={`error ${this.props.validated === false || (this.props.validated === true && this.props.mensagem !== "") ? "hide" : ""}`}>
                                <strong>Este campo é obrigatório</strong>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Passo2;
