import React, {Component} from "react";
import Main from "../../Main";
import Passo1 from "./Passo1";
import Passo2 from "./Passo2";
import Sucesso from "./Sucesso";
import Acessorios from "../Acessorios";
import NossasOfertas from "../OfertasLista";
import ImagePopup from "../../../assets/images/popup-revisao.jpg";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {sendRevisao} from "../../../actions/RevisoesAction";

import ScriptsSend from "../../helpers/ScriptsSend";

const Popup = () => {
    return (
        <div id="lacrador" className="popup-main">
            <div className="popup-overlay"></div>
            <div className="popup-out" align="center">
                <div className="popup-inner">
                    <div className="popup-body image">
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=5581997880056&text=Oi" rel="noopener noreferrer">
                            <img src={ImagePopup} className="img-fluid" alt="Popup"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};

class FormGeral extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentStep: 1,
            nome: "",
            email: "",
            telefone: "",
            hora1: "",
            data1: "",
            hora2: "",
            data2: "",
            km: "",
            modelo: "",
            ano: "",
            placa: "",
            mensagem: "",
            validated: false,
            sending: false,
            // this._next = this._next.bind(this),
            // this._prev = this._prev.bind(this)
        };

        this.method = "send-agendamento";

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (event) => {
        const {name, value} = event.target;
        this.setState({
            [name]: value,
        });
    };

    handleSubmit(event) {
        event.preventDefault();
        const {list_settings} = this.props;
        this.setState({
            sending: true,
        });


        const formData = new FormData();
        formData.append("carro", this.props.carro);
        formData.append("to_address", ScriptsSend.getEmailsByKey(list_settings, this.method));
        formData.append("form", ScriptsSend.getFormSlug(list_settings, this.method));
        formData.append("nome", this.state.nome);
        formData.append("email", this.state.email);
        formData.append("telefone", this.state.telefone);
        formData.append("hora1", this.state.hora1);
        formData.append("data1", this.state.data1);
        formData.append("hora2", this.state.hora2);
        formData.append("data2", this.state.data2);
        formData.append("km", this.state.km);
        formData.append("modelo", this.state.modelo);
        formData.append("ano", this.state.ano);
        formData.append("placa", this.state.placa);
        formData.append("mensagem", this.state.mensagem);

        this.props.sendRevisao(this.method, formData);
    }

    _next = (event) => {
        let currentStep = this.state.currentStep;
        let validate = this.hasError(currentStep);
        if (validate == 0) {
            currentStep = currentStep >= 2 ? 3 : currentStep + 1;
            this.setState({
                currentStep: currentStep,
                validated: false,
            });
            if (currentStep == 3) {
                this.handleSubmit(event);
            }
        }
    };

    _prev = () => {
        let currentStep = this.state.currentStep;
        currentStep = currentStep <= 1 ? 1 : currentStep - 1;
        this.setState({
            currentStep: currentStep,
        });
    };

    previousButton() {
        let currentStep = this.state.currentStep;
        if (currentStep == 2) {
            return (
                <button className="btn-enviar" type="button" onClick={this._prev}>
                    Voltar
                </button>
            );
        }
        return null;
    }

    nextButton() {
        let currentStep = this.state.currentStep;
        if (currentStep < 3) {
            return (
                <button className="btn-enviar" type="button" onClick={this._next}>
                    Avançar
                </button>
            );
        }
        return null;
    }

    hasError(currentStep) {
        var itemsInputs = document.querySelectorAll(".form-passo" + currentStep + " input");
        var itemsSelecteds = document.querySelectorAll(".form-passo" + currentStep + " select");
        var itemsTextAreas = document.querySelectorAll(".form-passo" + currentStep + " textarea");
        let error = 0;

        itemsInputs.forEach((item, key) => {
            if (item.value == "") {
                item.classList.add("error");
                error = error + 1;
            }
            if (item.value != "") {
                if (item.classList.contains("error")) {
                    item.classList.remove("error");
                }
            }
        });
        itemsSelecteds.forEach((item, key) => {
            if (item.value == "") {
                item.classList.add("error");
                error = error + 1;
            }
            if (item.value != "" && item.classList.contains("error")) {
                item.classList.remove("error");
            }
        });
        itemsTextAreas.forEach((item, key) => {
            if (item.value == "") {
                item.classList.add("error");
                error = error + 1;
            }
            if (item.value != "" && item.classList.contains("error")) {
                item.classList.remove("error");
            }
        });
        if (error > 0)
            this.setState({
                validated: true,
            });
        return error;
    }

    render() {
        const {success_revisao} = this.props;
        if (this.state.currentStep == 1) {
            var style1 = {
                background: "#B40C0C",
            };
        }
        if (this.state.currentStep == 2) {
            var style2 = {
                background: "#B40C0C",
            };
        }
        if (this.state.currentStep == 3) {
            var style3 = {
                background: "#B40C0C",
            };
        }

        return (
            <React.Fragment>
                <Main>
                    <div className="container revisao-page">
                        <div className="titulo">
                            <p className="nome">AGENDE SUA </p>
                            <hr className="linha-titulo" />
                            <h2>REVISÃO</h2>
                        </div>
                        <div className="revisao">
                            <div className="etapas">
                                <div className="circle" style={style1}></div>
                                <hr />
                                <div className="circle" style={style2}></div>
                                <hr />
                                <div className="circle" style={style3}></div>
                            </div>
                            {/* <span className="progress-step">Step {this.state.step}</span>
                            <progress className="progress" style={style}></progress> */}

                            <form onSubmit={this.handleSubmit} id={"FormAgendamento"}>
                                <Passo1 currentStep={this.state.currentStep} handleChange={this.handleChange} validated={this.state.validated} nome={this.state.nome} email={this.state.email} telefone={this.state.telefone} hora1={this.state.hora1} data1={this.state.data1} hora2={this.state.hora2} data2={this.state.data2} km={this.state.km} />

                                <Passo2 currentStep={this.state.currentStep} handleChange={this.handleChange} validated={this.state.validated} modelo={this.state.modelo} ano={this.state.ano} placa={this.state.placa} mensagem={this.state.mensagem} />

                                <Sucesso currentStep={this.state.currentStep} handleChange={this.handleChange} validated={this.state.validated} sending={this.state.sending} success_revisao={success_revisao} />

                                {this.previousButton()}
                                {this.nextButton()}
                            </form>
                        </div>
                    </div>
                    {/* <Acessorios /> */}
                    {/* <NossasOfertas /> */}
                </Main>
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    list_settings: state.settings.get("list_settings"),
    error_revisao: state.revisoes.get("error_revisao"),
    success_revisao: state.revisoes.get("success_revisao"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({sendRevisao}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FormGeral);

// export default FormGeral
