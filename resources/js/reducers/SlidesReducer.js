import {fromJS} from 'immutable'

const PLURAL = 'SLIDES'
// const SINGULAR = 'SLIDE'

const INITIAL_STATE = fromJS({
  list_slides: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_slides', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_slides', fromJS([]))
    default:
      return state
  }
}