import {fromJS} from 'immutable'

const PLURAL = 'SETTINGS'

const INITIAL_STATE = fromJS({
  list_settings: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_settings', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_settings', fromJS([]))
    default:
      return state
  }
}