import {fromJS} from 'immutable'

const PLURAL = 'CONTATOS'
const SINGULAR = 'CONTATO'

const INITIAL_STATE = fromJS({
  list_contatos: [],
  meta: [],
  item_contato: null,
  error_contato: [],
  success_contato: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_contatos', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_contato', fromJS(action.payload))
    case `SET_ERROR_${SINGULAR}`:
      return state.set('error_contato', fromJS(action.payload))
    case `SET_SUCCESS_${SINGULAR}`:
      return state.set('success_contato', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_contato', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_contatos', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_contato', fromJS([]))
        .set('error_contato', fromJS([]))
        .set('success_contato', fromJS([]))
    default:
      return state
  }
}