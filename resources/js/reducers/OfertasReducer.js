import {fromJS} from 'immutable'

const PLURAL = 'OFERTAS'
const SINGULAR = 'OFERTA'

const INITIAL_STATE = fromJS({
  list_ofertas: [],
  meta: [],
  item_oferta: null
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_ofertas', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_oferta', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_oferta', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_ofertas', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_oferta', fromJS([]))
    default:
      return state
  }
}