import {fromJS} from 'immutable'

const PLURAL = 'REVISOES'
const SINGULAR = 'REVISAO'

const INITIAL_STATE = fromJS({
  list_revisoes: [],
  meta: [],
  item_revisao: null,
  error_revisao: [],
  success_revisao: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_revisoes', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_revisao', fromJS(action.payload))
    case `SET_ERROR_${SINGULAR}`:
      return state.set('error_revisao', fromJS(action.payload))
    case `SET_SUCCESS_${SINGULAR}`:
      return state.set('success_revisao', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_revisao', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_revisoes', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_revisao', fromJS([]))
        .set('error_revisao', fromJS([]))
        .set('success_revisao', fromJS([]))
    default:
      return state
  }
}