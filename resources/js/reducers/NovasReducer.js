import {fromJS} from 'immutable'

const PLURAL = 'NOVAS'
const SINGULAR = 'NOVA'

const INITIAL_STATE = fromJS({
  list_novas: [],
  meta: [],
  item_nova: null
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_novas', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_nova', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_nova', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_novas', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_nova', fromJS([]))
    default:
      return state
  }
}