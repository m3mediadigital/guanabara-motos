import {fromJS} from 'immutable'

const PLURAL = 'CONSORCIOS'
const SINGULAR = 'CONSORCIO'

const INITIAL_STATE = fromJS({
  list_consorcios: [],
  meta: [],
  item_consorcio: null
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_consorcios', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_consorcio', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_consorcio', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_consorcios', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_consorcio', fromJS([]))
    default:
      return state
  }
}