import React from "react";

// import { HashRouter } from 'react-router-dom'

import Routes from "./Routes";

function App() {
    return (
        <>
            <Routes />
        </>
    );
}

export default App;
