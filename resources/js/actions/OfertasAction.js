import SimpleCrud from "../services/simpleCrud";

const PLURAL = "OFERTAS";
const SINGULAR = "OFERTA";
const LIST_URL = "/ofertas";
const COMPANY = "30";
const METHOD_LIST = "getOfertas";
const METHOD_GET = "getOferta";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getOferta = (slug) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.get(METHOD_GET, slug, dispatch, SINGULAR, LIST_URL);
    };
};

export const getListOfertas = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
