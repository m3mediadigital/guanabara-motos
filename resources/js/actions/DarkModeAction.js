
export const activeDarkMode = () => {
    localStorage.setItem('dark_mode', 1)
    window.location.reload(false);
}

export const deactiveDarkMode = () => {
    localStorage.setItem('dark_mode', 0)
    window.location.reload(false);
}

export const isDarkMode = () => localStorage.getItem('dark_mode');