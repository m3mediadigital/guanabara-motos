import SimpleCrud from "../services/simpleCrud";

const PLURAL = "SETTINGS";
const COMPANY = "30";
const METHOD_LIST = "getSettings";
export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getListSettings = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
