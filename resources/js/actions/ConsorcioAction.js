import SimpleCrud from "../services/simpleCrud";

const PLURAL = "CONSORCIOS";
const SINGULAR = "CONSORCIO";
const LIST_URL = "/consorcios";
const METHOD_LIST = "getConsorcios";
const METHOD_GET = "getConsorcio";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getConsorcio = (slug) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.get(METHOD_GET, slug, dispatch, SINGULAR, LIST_URL);
    };
};

export const getListConsorcios = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
