import SimpleCrud from "../services/simpleCrud";

const PLURAL = "SEMINOVOS";
const SINGULAR = "SEMINOVO";
const LIST_URL = "/seminovos";
const COMPANY = "30";
const METHOD_LIST = "getSeminovos";
const METHOD_GET = "getSeminovo";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getSeminovo = (slug) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.get(METHOD_GET, slug, dispatch, SINGULAR, LIST_URL);
    };
};

export const getListSeminovos = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
