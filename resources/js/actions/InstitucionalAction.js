import SimpleCrud from "../services/simpleCrud";

const PLURAL = "INSTITUCIONAL";
const COMPANY = "30";
const METHOD_LIST = "getInstitucional";
export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getInstitucional = (per_page = null, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
