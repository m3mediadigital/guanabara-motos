import SimpleCrud from '../services/simpleCrud'

const PLURAL = 'REVISOES'
const SINGULAR = 'REVISAO'
const LIST_URL = '/revisoes'
const COMPANY = '30'

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    }
}


export const sendRevisao = (method, request) => {

    return dispatch => {
        dispatch(unset())

        SimpleCrud.add(
            method,
            request,
            dispatch,
            SINGULAR
        )
    }
}
