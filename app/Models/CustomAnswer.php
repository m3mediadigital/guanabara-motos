<?php

namespace App\Models;

class CustomAnswer extends AppDescomplicarModel
{
	protected $table = 'custom_answer';
	
    public function custom_question()
	{
	    return $this->belongsTo('App\Models\CustomQuestion', 'custom_questions_id', 'id');
	}

	public function custom_options()
	{
		return $this->belongsTo('App\Models\CustomOption', 'value', 'id');
	}

    public function showroom()
	{
	    return $this->belongsTo('App\Models\Showroom', 'showroom_id', 'id');
	}

    public function lead()
	{
	    return $this->belongsTo('App\Models\Lead', 'leads_id', 'id');
	}

	public function catalog()
	{
	    return $this->belongsTo('App\Models\Catalog', 'catalog_id', 'id');
	}

	public function page()
	{
	    return $this->belongsTo('App\Models\Page', 'page_id', 'id');
	}

 //    public function lead_status()
	// {
	//     return $this->belongsTo('App\Models\LeadStatus', 'leads_status_id', 'id');
	// }

}
