<?php

namespace App\Models;

class Highlight extends AppDescomplicarModel
{
	protected $table = 'highlights_models';
	
    public function model()
	{
	    return $this->belongsTo('App\Models\Model', 'models_id', 'id');
	}
	
}
