<?php

namespace App\Models;

class PageGallery extends AppDescomplicarModel
{
	protected $table = 'pages_gallery';
	
    public function page()
	{
	    return $this->belongsTo('App\Models\Page', 'pages_id', 'id');
	    // return $this->belongsTo('App\Models\Type', 'type_id', 'id')->withDefault();
	}
}
