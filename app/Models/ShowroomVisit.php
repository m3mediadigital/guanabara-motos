<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ShowroomVisit extends AppDescomplicarModel
{
    protected $table = 'showroom_visits';
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
	
    public function showroom()
	{
	    return $this->belongsTo('App\Models\Showroom', 'showroom_id', 'id');
	}

    public static function customCount($carro_id)
    {
        $sv = new ShowroomVisit();
        $sv->showroom_id = $carro_id;
        $sv->ip = $_SERVER['REMOTE_ADDR'];
        try {
            return $sv->save();
        } catch (\Exception $ex) { return null; }
    }

}
