<?php

namespace App\Models;

class AppSim3SeminovosModel 
{

    public static function allSeminovos()
    {
        $url = 'http://www.seminovosautobraz.com/allSeminovos';

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec( $ch );

        return json_decode($response);
    }


}
