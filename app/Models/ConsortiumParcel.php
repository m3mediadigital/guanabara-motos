<?php

namespace App\Models;

class ConsortiumParcel extends AppDescomplicarModel
{
	protected $table = 'consortium_parcels';
	
    public function consortium()
	{
	    return $this->belongsTo('App\Models\Consortium', 'consortium_id', 'id');
	}
}
