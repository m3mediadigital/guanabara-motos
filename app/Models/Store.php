<?php

namespace App\Models;

class Store extends AppDescomplicarModel
{
	protected $table = 'stores';
    
    public static function customFetchAll() 
    {
        $stores = self::query()
            ->select( 
                '*',
                \Illuminate\Support\Facades\DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
            )
            ->where([
                ['companies_id', self::$COMPANY_ID],
            ])
            ->get();
        return $stores;
    }
	
    public static function customStoreBySlug($slug=null) 
    {
        $store = self::query()
            ->select( 
            	'*',
            	\Illuminate\Support\Facades\DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
            )
            ->where([
                ['companies_id', self::$COMPANY_ID],
                ['slug', $slug],
            ])
            ->orderBy('id', 'asc')
        	->first();

        return $store;
    }
}
