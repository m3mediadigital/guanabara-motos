<?php

namespace App\Models;

class Model extends AppDescomplicarModel
{
	protected $table = 'models';
	
    public function brand()
	{
	    return $this->belongsTo('App\Models\Brand', 'brands_id', 'id');
	}
	
    public function type()
	{
	    return $this->belongsTo('App\Models\Type', 'type_id', 'id');
	}
	
    public function body()
	{
	    return $this->belongsTo('App\Models\Body', 'bodies_id', 'id');
	}
	
    public function cc()
	{
	    return $this->belongsTo('App\Models\Cc', 'cc_id', 'id');
	}
	
    public function fuel()
	{
	    return $this->belongsTo('App\Models\Fuel', 'fuels_id', 'id');
	}
	
    public function motorization()
	{
	    return $this->belongsTo('App\Models\Motorization', 'motorization_id', 'id');
	}
	
    public function transmission()
	{
	    return $this->belongsTo('App\Models\Transmission', 'transmissions_id', 'id');
	}

    public function colors()
	{
		return $this->hasMany('App\Models\Color', 'models_id', 'id');
	}

    public function gallery()
	{
		return $this->hasMany('App\Models\ModelGallery', 'models_id', 'id');
	}

	public function highlight()
	{
		return $this->hasMany('App\Models\Highlight', 'models_id', 'id');
	}

}
