<?php

namespace App\Models;

class PageHasCategory extends AppDescomplicarModel
{
	protected $table = 'pages_has_pages_categories';
	
    public function page()
	{
	    return $this->belongsTo('App\Models\Page', 'pages_id', 'id');
	    // return $this->belongsTo('App\Models\Type', 'type_id', 'id')->withDefault();
	}
}
