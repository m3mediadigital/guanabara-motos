<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Slide extends AppDescomplicarModel
{
	protected $table = 'slides';
	
    public function showroom()
	{
	    return $this->belongsTo('App\Models\Showroom', 'showroom_id', 'id');
	}
	
    public static function customFetchAll() 
    {
        $slides = self::query()
        	->select(
                'id',
                'type',
                'link',
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image_mobile) as image_mobile'),

                // DB::raw('CONCAT("http://localhost/carros-2016/upload/", image) as image'),
                // DB::raw('CONCAT("http://localhost/carros-2016/upload/", image_mobile) as image_mobile'),
                'showroom_id',
                'finish_at'
            )
            ->where([
                ['companies_id', self::$COMPANY_ID],
                ['active', 1],
                ['finish_at', '>=', date('Y-m-d')],
            ])
            ->orderBy('position', 'asc')
        	->get();

        foreach($slides as $k => $slide) {
            if( in_array($slide->type, ['showroom', 'novo']) ){
                if(!empty($slide->showroom) && ($slide->type == 'novo' || ($slide->type == 'showroom' && $slide->showroom->finish_at >= date('Y-m-d 23:59:59')))) {
                    self::singleCustomKey($slide->showroom);
                    $slide->showroom->full_name = $slide->showroom->model->complete_name.( $slide->showroom->version ? ' '.$slide->showroom->version->complete_name : '');
                } else {
                    unset($slides[$k]);
                }
            }
        }

        return $slides;
    }

}
