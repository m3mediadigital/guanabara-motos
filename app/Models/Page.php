<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Page extends AppDescomplicarModel
{
	protected $table = 'pages';

    public function custom_answers()
	{
		return $this->hasMany('App\Models\CustomAnswer', 'page_id', 'id');
	}

    public function gallery()
	{
		return $this->hasMany('App\Models\PageGallery', 'pages_id', 'id');
	}

    public function pages_has_pages_categories()
    {
        return $this->hasMany('App\Models\PageHasCategory', 'pages_id', 'id');
    }

	private static function customBuildQuery($preffix=false)
	{
		if( $preffix !== false )
			$preffix = 'pages.';
		else
			$preffix = '';

		$query = self::query()
        	->select(
                $preffix.'id',
                $preffix.'slug',
                $preffix.'titulo',
                $preffix.'subtitulo',
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", '.$preffix.'imagem_destaque) as imagem'),
                $preffix.'conteudo',
                $preffix.'companies_id'
            )
            ->with([
                'gallery' => function($q) {
                    $q->select( 
                        'pages_gallery.pages_id',
                        'pages_gallery.path'
                        // DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", pages_gallery.path) as imagem')
                    );
                },
                'custom_answers' => function($q) {
                    $q->with([ 'custom_question' => function($q) {
                            $q->select([
                                'custom_questions.id',
                                'custom_questions.slug',
                                'custom_questions.type'
                            ]);
                        }
                    ]);
                }
            ]);

        return $query;
	}

    private static function customAll($params = null)
    {
    	$conditions = [
            ['companies_id', self::$COMPANY_ID]
        ];

        if( !empty($params) ){
	        if( is_array($params) ){
	        	$conditions[] = $params;
	        }else if( is_string($params) ){
	        	$conditions[] = ['slug', $params];
	        }
	    }

        $query = self::customBuildQuery();

        $pages = $query->where($conditions)
            ->get();

        return $pages;
    }

    public static function customFetchAll()
    {
    	return self::customAll();
    }

    public static function customFetchAllByNotCategory()
    {

        $query = self::customBuildQuery(true);

        $pages = $query->doesntHave('pages_has_pages_categories')
            ->where([['pages.companies_id', '=', self::$COMPANY_ID]])
            ->orderBy('id', 'desc')
            ->get();

        return $pages;
    }

    public static function customGetBySlug($slug)
    {
    	$dados = self::customAll($slug);
        $page = null;
        if( count($dados) > 0 )
            $page = $dados[0];
        // dump($page);
        return $page;
    }

    public static function customFetchAllByCategorySlug($category_slug = null, $id = null)
    {
        if(!$category_slug) {
            throw new \Exception("Não foi informado a o id da categoria desejada.", 1);
        }

        if( !is_string($category_slug) ) {
            throw new \Exception("Não foi informado um slug de categoria válido.", 1);
        }

        $query = self::customBuildQuery(true);

        $pages = $query->join('pages_has_pages_categories', function ($join) {
	            $join->on('pages.id', '=', 'pages_has_pages_categories.pages_id');
	        })
            ->join('pages_categories', function ($join) use ($category_slug) {
	            $join->on('pages_categories.id', '=', 'pages_has_pages_categories.pages_categories_id')
	            	->where('pages_categories.category', '=', $category_slug);
	        })
            ->where([
                ['pages.companies_id', self::$COMPANY_ID]
            ])
            ->orderBy('id', 'desc');

        if($id) {
            $pages = $pages->find($id);
        } else {
            $pages = $pages->first();
        }

        if( $pages ) {
            self::singleCustomKey($pages);
        }

        return $pages;
    }
}
