<?php

namespace App\Models;

class FinancingTableParcel extends AppDescomplicarModel
{
	protected $table = 'financing_table_parcels';
	
    public function financing_table()
	{
	    return $this->belongsTo('App\Models\FinancingTable', 'financing_table_id', 'id');
	}

}
