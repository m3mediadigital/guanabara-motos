<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Company extends AppDescomplicarModel
{
	protected $table = 'companies';
	
   	public static function getKeyCompany() 
    {
    	$company_key = self::query()
        	->select(
                'key_code'
            )
            ->where([
                ['id', self::$COMPANY_ID]
            ])
        	->pluck(
                'key_code'
            )->first();
        return $company_key;
    }

}
