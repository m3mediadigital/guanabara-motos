<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ComparisonQuestion extends AppDescomplicarModel
{
	protected $table = 'comparisons_questions';

    public function comparison()
    {
        return $this->belongsTo('App\Models\Comparison', 'comparisons_id', 'id');
    }

}
