<?php

namespace App\Models;

class ModelGallery extends AppDescomplicarModel
{
	protected $table = 'models_gallery';
	
    public function model()
	{
	    return $this->belongsTo('App\Models\Model', 'models_id', 'id');
	}

    public function youtube()
	{
	    return $this->belongsTo('App\Models\Youtube', 'youtubes_id', 'id');
	}

}
