<?php

namespace App\Models;

class CustomQuestion extends AppDescomplicarModel
{
	protected $table = 'custom_questions';
	
    public function form()
	{
	    return $this->belongsTo('App\Models\Form', 'forms_id', 'id');
	}

    public function custom_options()
	{
	    return $this->hasMany('App\Models\CustomOption', 'id', 'custom_questions_id');
	}

}
