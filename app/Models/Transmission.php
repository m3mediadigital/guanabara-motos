<?php

namespace App\Models;

class Transmission extends AppDescomplicarModel
{
	protected $table = 'transmissions';
	
    public function type()
	{
	    return $this->belongsTo('App\Models\Type', 'type_id', 'id');
	    // return $this->belongsTo('App\Models\Type', 'type_id', 'id')->withDefault();
	}
}
