<?php

namespace App\Http\Controllers;


use App\Models\AppDescomplicarModel;
use App\Models\Catalog;
use App\Models\CatalogCategory;
use App\Models\Company;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Showroom;
use App\Models\Slide;
use App\Models\Store;

class ApiController extends Controller
{
    use \App\Traits\Email;

    public function __construct()
    {
        AppDescomplicarModel::$COMPANY_ID = (int)40;
        AppDescomplicarModel::$COMPANY_KEY = Company::getKeyCompany();
    }

    public function slides()
    {
        return response()->json(Slide::customFetchAll());
    }

    public function settings()
    {
        return response()->json(Setting::customFetchAll());
    }

    public function lojas()
    {
        return response()->json(Store::customFetchAll());
    }

    public function novas()
    {
        return response()->json(Showroom::customFetchAllNovos());
    }

    public function novasList()
    {
        return response()->json(Showroom::customFetchAllNovosIndex());
    }

    public function nova()
    {
        return response()->json(Showroom::customNovoBySlug(request()->slug));
    }

    public function ofertas()
    {
        return response()->json(Showroom::customListAllOfertas());
    }

    public function oferta()
    {
        return response()->json(Showroom::customOfertaBySlug(request()->slug));
    }

    public function seminovos()
    {
        return response()->json(Showroom::customListAllSeminovos(request()->order , request()->limit));
    }

    public function seminovo()
    {
        return response()->json(Showroom::customSeminovoBySlug(request()->slug));
    }

    public function vendasDiretas()
    {
        return response()->json(Showroom::customFetchAllVendasDiretas(null, request()->order , request()->limit ));
    }

    public function vendaDireta()
    {
        return response()->json(Showroom::customVendaDiretaBySlug(request()->slug));
    }

    public function acessorios()
    {
        return response()->json(Catalog::customFetchAll(request()->limit));
    }

    public function acessorio()
    {
        return response()->json(Catalog::customBySlug(request()->slug));
    }

    public function consorcios()
    {
        return response()->json(Showroom::customFetchAllConsorcios(null, request()->limit));
    }

    public function institucional()
    {
        $pages = [];
        $pages = Page::customFetchAllByNotCategory();

        return response()->json($pages);
    }

    //Exclusivo Pernambuco
    public function categorias()
    {      
        return response()->json(CatalogCategory::where('companies_id', (int)request()->company_id)->get());
    }

    public function categoria()
    {
        $categoriaId = CatalogCategory::where([['companies_id', (int)request()->company_id], ['slug', request()->slug]])->first();
        $acessorio   = Catalog::customById($categoriaId->id);

        return response()->json($acessorio);
    }
}
