<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contato extends Mailable
{
    use Queueable, SerializesModels;
    
    public $layoutDefault = 'mail.contato';
    public $layoutDescomplicar = 'mail.descomplicar';

    public $requestData;
    public $layout;

    /**

     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $layout = null)
    {
        $this->requestData = $request;
        // dump($request);

        if(isset($request->oferta)){
            unset($this->requestData['carro']);
            $this->requestData['oferta'] = $request->oferta;
        }

        if(isset($request->novo)){
            unset($this->requestData['carro']);
            $this->requestData['novo'] = $request->novo;
        }

        if(isset($request->produto)){
            unset($this->requestData['carro']);
            $this->requestData['produto'] = $request->produto;
        }

        if(isset($request->vendas_diretas)){
            unset($this->requestData['carro']);
            $this->requestData['vendas_diretas'] = $request->vendas_diretas;
        }

        if(isset($request->consorcio)){
            unset($this->requestData['carro']);
            $this->requestData['consorcio'] = $request->consorcio;
        }

        if(isset($request['descomplicar'])){
            $this->requestData['descomplicar'] = $request['descomplicar'];
        } else {
            $this->requestData['descomplicar'] = false;
        }



        switch ($layout) {
            // case 'mail.contato':
            case $this->layoutDefault:
                $this->layout = $layout;
                break;
            default:
                $this->layout = $this->layoutDefault;
                break;
        }



    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(empty($this->requestData['descomplicar']) || $this->requestData['descomplicar'] == false){
            // dd($this->requestData);
            return $this->from(env('MAIL_FROM_ADDRESS', 'envio@novam3.com'))
                ->markdown('mail.contato')->subject($this->requestData['subject']);
        } else {
            return $this->view($this->layout);
        }
    }
}
