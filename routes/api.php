<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/getSlides',    [ApiController::class, 'slides'])->name('slides');
Route::get('/getSettings',  [ApiController::class, 'settings'])->name('settings');
Route::get('/getLojas',     [ApiController::class, 'lojas'])->name('lojas');
Route::get('/getNovasIndex',[ApiController::class, 'novasList'])->name('novasList');
Route::get('/getNovas',     [ApiController::class, 'novas'])->name('novas');
Route::get('/getNova',      [ApiController::class, 'nova'])->name('nova');
Route::get('/getOfertas',   [ApiController::class, 'ofertas'])->name('ofertas');
Route::get('/getOferta',    [ApiController::class, 'oferta'])->name('oferta');
Route::get('/getVendasDiretas', [ApiController::class, 'vendasDiretas'])->name('vendasDiretas');
Route::get('/getVendasDireta',  [ApiController::class, 'vendasDireta'])->name('vendasDireta');
Route::get('/getConsorcios',    [ApiController::class, 'consorcios'])->name('consorcios');
Route::get('/getSeminovos',     [ApiController::class, 'seminovos'])->name('seminovos');
Route::get('/getSeminovo',      [ApiController::class, 'seminovo'])->name('seminovo');
Route::get('/getAcessorios',    [ApiController::class, 'acessorios'])->name('acessorios');
Route::get('/getAcessorio',     [ApiController::class, 'acessorio'])->name('acessorio');
Route::get('/getInstitucional', [ApiController::class, 'institucional'])->name('institucional');
Route::get('/getCategorias',    [ApiController::class, 'categorias'])->name('categorias');
Route::get('/getCategoria',     [ApiController::class, 'categoria'])->name('categoria');






Route::post('/send-oferta',     [ApiController::class, 'propostaOferta'])->name('propostaOferta');
Route::post('/send-nova',       [ApiController::class, 'propostaNovo'])->name('propostaNovo');
Route::post('/send-seminova',   [ApiController::class, 'propostaSeminovo'])->name('propostaSeminovo');
Route::post('/send-consorcio',  [ApiController::class, 'propostaConsorcio'])->name('propostaConsorcio');
Route::post('/send-contato',    [ApiController::class, 'contato'])->name('contato');
Route::post('/send-seguros',    [ApiController::class, 'seguros'])->name('seguros');
Route::post('/send-agendamento',[ApiController::class, 'realizarAgendamento'])->name('realizarAgendamento');
Route::post('/send-trabalhe_conosco', [ApiController::class, 'trabalheConosco'])->name('trabalheConosco');
Route::post('/send-acessorio',        [ApiController::class, 'propostaAcessorios'])->name('propostaAcessorios');

